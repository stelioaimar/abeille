#include "gestreproduction.h"
#include "ui_gestreproduction.h"

GestReproduction::GestReproduction(QWidget *parent, QString currentUser) :
    QWidget(parent),
    ui(new Ui::GestReproduction)
{
    ui->setupUi(this);
    idUser=currentUser;

    ui->label_debut->hide();
    ui->dateEdit_debut->hide();
    ui->label_fin->hide();
    ui->dateEdit_fin->hide();
    ui->dateTimeEdit_colonisation->setDateTime(QDateTime::currentDateTime());
    ui->dateTimeEdit_installation->setDateTime(QDateTime::currentDateTime());
    modeleSelection = new QSqlQueryModel;
    modeleConsulter = new QSqlQueryModel;
    modeleRecuperation = new QSqlQueryModel;
    modeleCritere = new QSqlQueryModel;
    modeleProfil = new QSqlQueryModel;
    afficherInfo();
   // afficherInfo2();
    effacerChamps();
    affich();
    requeteSelection.prepare("select DISTINCT numeroTruie FROM gestRepro");
    OutilsMimes::customComboBox(ui->comboBox_numeroTruie,requeteSelection);
    requeteSelection.prepare("select DISTINCT numeroVerrat FROM gestRepro");
    OutilsMimes::customComboBox(ui->comboBox_numeroVerrat,requeteSelection);
    requeteSelection.prepare("select DISTINCT resultatsaillie FROM gestRepro");
    OutilsMimes::customComboBox(ui->comboBox_resultatSaillie,requeteSelection);

    //pour cacher certains objets préexistants
    ui->comboBox_resultatSaillie->hide();
    ui->comboBox_observation->hide();
    ui->comboBox_numeroTruie->hide();
    ui->comboBox_numeroVerrat->hide();
    ui->dateTimeEdit_miseBas->hide();
    ui->dateTimeEdit_sevrage->hide();
    ui->spinBox_aliment->hide();
    ui->label_objet_4->hide();
    ui->label_objet_5->hide();
    ui->label_objet_6->hide();
    ui->label_nbr_Pj->hide();
    ui->label_nbr_Pj_2->hide();
    ui->label_nbr_Pj_3->hide();
    ui->label_expediteur->hide();
    ui->tableWidget_total->hide();
    ui->frameNotice->hide();
    ui->spinBox_numRuche->hide();
}

GestReproduction::~GestReproduction()
{
    delete ui;
}

void GestReproduction::changerOgletActif(int ind)
{
    ui->tabWidget->setCurrentIndex(ind);
}

void GestReproduction::effacerChamps()
{
    ui->comboBox_TypeRuche->clearEditText();
    ui->lineEdit_numRuche->clear();
    ui->spinBox_aliment->clear();
}

void GestReproduction::afficherInfo()
{
   requeteSelection.prepare("SELECT libelleTypeR, numRuche, date_format(dateInstallation,'%d/%m/%Y %H:%i:%S'), "
                                        "date_format(dateColonisation,'%d/%m/%Y %H:%i:%S'), idRuche "
                                        "FROM ruche "
                                        "INNER JOIN typeruche ON ruche.idTypeR = typeruche.idTypeR");
                                        //"WHERE date(dateEnregistrement) ='"+QDate::currentDate().toString("yyyy-MM-dd")+"'");
        if (!requeteSelection.exec())
       QMessageBox::information(this,"Erreur selection ruche",requeteSelection.lastError().text());
       modeleSelection->setQuery(requeteSelection);
       ui->tableWidget_enregistrment->setColumnCount(4);
       QStringList entete;
       entete<<"TYPE DE RUCHE"<<"NUMERO DE LA RUCHE"<<"DATE D'INSTALLATION"<<"DATE DE COLONISATION";
       ui->tableWidget_enregistrment->setHorizontalHeaderLabels(entete);
        for (int i=0; i<requeteSelection.size(); i++)
        {
            ui->tableWidget_enregistrment->setRowCount(i+1);
            QTableWidgetItem *libelleTypeR = new QTableWidgetItem(modeleSelection->record(i).value(0).toString());
            libelleTypeR->setFlags(Qt::ItemIsEnabled);
            libelleTypeR->setTextAlignment(Qt::AlignCenter);
            libelleTypeR->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *numRuche = new QTableWidgetItem(modeleSelection->record(i).value(1).toString());
            numRuche->setFlags(Qt::ItemIsEnabled);
            numRuche->setTextAlignment(Qt::AlignCenter);
            numRuche->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *dateInstallation = new QTableWidgetItem(modeleSelection->record(i).value(2).toString());
            dateInstallation->setFlags(Qt::ItemIsEnabled);
            dateInstallation->setTextAlignment(Qt::AlignCenter);
            dateInstallation->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *dateColonisation = new QTableWidgetItem(modeleSelection->record(i).value(3).toString());
            dateColonisation->setFlags(Qt::ItemIsEnabled);
            dateColonisation->setTextAlignment(Qt::AlignCenter);
            dateColonisation->setForeground(QBrush(QColor(255,255,255)));

            ui->tableWidget_enregistrment->setItem(i,0,libelleTypeR);
            ui->tableWidget_enregistrment->setItem(i,1,numRuche);
            ui->tableWidget_enregistrment->setItem(i,2,dateInstallation);
            ui->tableWidget_enregistrment->setItem(i,3,dateColonisation);

        }
        OutilsMimes::customizeTableWidget(ui->tableWidget_enregistrment);
        ui->pushButtonEnregistrer->setEnabled(true);
        ui->pushButtonModifier->setDisabled(true);
}

void GestReproduction::afficherInfo2()
{
       ui->tableWidgetcons->setColumnCount(4);
       QStringList entete;
       entete<<"TYPE DE RUCHE"<<"NUMERO DE LA RUCHE"<<"DATE D'INSTALLATION"<<"DATE DE COLONISATION";
       if(requeteConsulter.size()==0){
           ui->tableWidgetcons->clear();
           ui->tableWidgetcons->setRowCount(0);
       }
       ui->tableWidgetcons->setHorizontalHeaderLabels(entete);
       //Entete des totaux
       QTableWidgetItem *itemeTotalPorte = new QTableWidgetItem("");
       itemeTotalPorte ->setFlags(Qt::ItemIsEnabled);
       itemeTotalPorte->setTextAlignment(Qt::AlignCenter);
       QTableWidgetItem *itemeTotalAliment = new QTableWidgetItem("");
       itemeTotalAliment->setFlags(Qt::ItemIsEnabled);
       itemeTotalAliment->setTextAlignment(Qt::AlignCenter);

       //construction des items des totaux
       ui->tableWidget_total->setItem(0,0,itemeTotalPorte);
       ui->tableWidget_total->setItem(0,1,itemeTotalAliment);
        for (int i=0; i<requeteConsulter.size(); i++)
        {
            ui->tableWidgetcons->setRowCount(i+1);
            QTableWidgetItem *itemBatiment = new QTableWidgetItem(modeleConsulter->record(i).value(0).toString());
            itemBatiment->setFlags(Qt::ItemIsEnabled);
            itemBatiment->setTextAlignment(Qt::AlignCenter);
            itemBatiment->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemNumeroTruie = new QTableWidgetItem(modeleConsulter->record(i).value(1).toString());
            itemNumeroTruie->setFlags(Qt::ItemIsEnabled);
            itemNumeroTruie->setTextAlignment(Qt::AlignCenter);
            itemNumeroTruie->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemDateSaillie = new QTableWidgetItem(modeleConsulter->record(i).value(2).toString());
            itemDateSaillie->setFlags(Qt::ItemIsEnabled);
            itemDateSaillie->setTextAlignment(Qt::AlignCenter);
            itemDateSaillie->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemResultSaillie = new QTableWidgetItem(modeleConsulter->record(i).value(3).toString());
            itemResultSaillie->setFlags(Qt::ItemIsEnabled);
            itemResultSaillie->setTextAlignment(Qt::AlignCenter);
            itemResultSaillie->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemDateRC = new QTableWidgetItem(modeleConsulter->record(i).value(4).toDateTime().toString("dd/MM/yyyy hh:mm:ss"));
            itemDateRC->setFlags(Qt::ItemIsEnabled);
            itemDateRC->setTextAlignment(Qt::AlignCenter);
            itemDateRC->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemDateMiseBas = new QTableWidgetItem(modeleConsulter->record(i).value(5).toDateTime().toString("dd/MM/yyyy hh:mm:ss"));
            itemDateMiseBas->setFlags(Qt::ItemIsEnabled);
            itemDateMiseBas->setTextAlignment(Qt::AlignCenter);
            itemDateMiseBas->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemPorte = new QTableWidgetItem(modeleConsulter->record(i).value(6).toString());
            itemPorte->setFlags(Qt::ItemIsEnabled);
            itemPorte->setTextAlignment(Qt::AlignCenter);
            itemPorte->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemDateSevre = new QTableWidgetItem(modeleConsulter->record(i).value(7).toDateTime().toString("dd/MM/yyyy hh:mm:ss"));
            itemDateSevre->setFlags(Qt::ItemIsEnabled);
            itemDateSevre->setTextAlignment(Qt::AlignCenter);
            itemDateSevre->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemAliment = new QTableWidgetItem(modeleConsulter->record(i).value(8).toString());
            itemAliment->setFlags(Qt::ItemIsEnabled);
            itemAliment->setTextAlignment(Qt::AlignCenter);
            itemAliment->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemNumeroVerrat = new QTableWidgetItem(modeleConsulter->record(i).value(9).toString());
            itemNumeroVerrat->setFlags(Qt::ItemIsEnabled);
            itemNumeroVerrat->setTextAlignment(Qt::AlignCenter);
            itemNumeroVerrat->setForeground(QBrush(QColor(255,255,255)));
            QTableWidgetItem *itemObservation = new QTableWidgetItem(modeleConsulter->record(i).value(11).toString());
            itemObservation->setFlags(Qt::ItemIsEnabled);
            itemObservation->setTextAlignment(Qt::AlignCenter);
            itemObservation->setForeground(QBrush(QColor(255,255,255)));

            ui->tableWidgetcons->setItem(i,0,itemBatiment);
            ui->tableWidgetcons->setItem(i,1,itemNumeroTruie);
            ui->tableWidgetcons->setItem(i,2,itemDateSaillie);
            ui->tableWidgetcons->setItem(i,3,itemResultSaillie);
            ui->tableWidgetcons->setItem(i,4,itemDateRC);
            ui->tableWidgetcons->setItem(i,5,itemDateMiseBas);
            ui->tableWidgetcons->setItem(i,6,itemPorte);
            ui->tableWidgetcons->setItem(i,7,itemDateSevre);
            ui->tableWidgetcons->setItem(i,8,itemAliment);
            ui->tableWidgetcons->setItem(i,9,itemNumeroVerrat);
            ui->tableWidgetcons->setItem(i,10,itemObservation);

            //affichage des totaux
            ui->tableWidget_total->item(0,0)->setText(QString::number(ui->tableWidget_total->item(0,0)->text().toInt()+itemPorte->text().toInt()));
            ui->tableWidget_total->item(0,1)->setText(QString::number(ui->tableWidget_total->item(0,1)->text().toInt()+itemAliment->text().toInt()));
            int nbreJoursRestantRC=QDateTime::currentDateTime().daysTo(modeleConsulter->record(i).value(4).toDateTime());
            if(nbreJoursRestantRC<=2 and modeleConsulter->record(i).value(4).toDateTime()>QDateTime::currentDateTime())
            {
                itemBatiment->setBackgroundColor(QColor(255,0,0));
                itemNumeroTruie->setBackgroundColor(QColor(255,0,0));
                itemDateSaillie->setBackgroundColor(QColor(255,0,0));
                itemResultSaillie->setBackgroundColor(QColor(255,0,0));
                itemDateRC->setBackgroundColor(QColor(255,0,0));
                itemDateMiseBas->setBackgroundColor(QColor(255,0,0));
                itemPorte->setBackgroundColor(QColor(255,0,0));
                itemDateSevre->setBackgroundColor(QColor(255,0,0));
                itemAliment->setBackgroundColor(QColor(255,0,0));
                itemNumeroVerrat->setBackgroundColor(QColor(255,0,0));
                itemObservation->setBackgroundColor(QColor(255,0,0));
            }

            int nbreJoursRestantMB=QDateTime::currentDateTime().daysTo(modeleConsulter->record(i).value(5).toDateTime());
            if(nbreJoursRestantMB<=2 and modeleConsulter->record(i).value(5).toDateTime()>QDateTime::currentDateTime())
            {
                itemBatiment->setBackgroundColor(QColor(255,0,255));
                itemNumeroTruie->setBackgroundColor(QColor(255,0,255));
                itemDateSaillie->setBackgroundColor(QColor(255,0,255));
                itemResultSaillie->setBackgroundColor(QColor(255,0,255));
                itemDateRC->setBackgroundColor(QColor(255,0,255));
                itemDateMiseBas->setBackgroundColor(QColor(255,0,255));
                itemPorte->setBackgroundColor(QColor(255,0,255));
                itemDateSevre->setBackgroundColor(QColor(255,0,255));
                itemAliment->setBackgroundColor(QColor(255,0,255));
                itemNumeroVerrat->setBackgroundColor(QColor(255,0,255));
                itemObservation->setBackgroundColor(QColor(255,0,0));
            }

            int nbreJoursRestantSevrage=QDateTime::currentDateTime().daysTo(modeleConsulter->record(i).value(7).toDateTime());
            if(nbreJoursRestantSevrage<=2 and modeleConsulter->record(i).value(7).toDateTime()>QDateTime::currentDateTime())
            {
                itemBatiment->setBackgroundColor(QColor(255, 85, 0));
                itemNumeroTruie->setBackgroundColor(QColor(255, 85, 0));
                itemDateSaillie->setBackgroundColor(QColor(255, 85, 0));
                itemResultSaillie->setBackgroundColor(QColor(255, 85, 0));
                itemDateRC->setBackgroundColor(QColor(255, 85, 0));
                itemDateMiseBas->setBackgroundColor(QColor(255, 85, 0));
                itemPorte->setBackgroundColor(QColor(255, 85, 0));
                itemDateSevre->setBackgroundColor(QColor(255, 85, 0));
                itemAliment->setBackgroundColor(QColor(255, 85, 0));
                itemNumeroVerrat->setBackgroundColor(QColor(255, 85, 0));
                itemObservation->setBackgroundColor(QColor(255,0,0));
            }
        }
        OutilsMimes::customizeTableWidget(ui->tableWidgetcons);
        ui->pushButtonEnregistrer->setEnabled(true);
        ui->pushButtonModifier->setDisabled(true);
}

void GestReproduction::affich()
{

    QStringList entete2;
    ui->tableWidget_total->setColumnCount(2);
    entete2<<"TOTAL PORTEE"<<"TOTAL ALIMENT";
    ui->tableWidget_total->setHorizontalHeaderLabels(entete2);
    ui->tableWidget_total->setRowCount(1);

    OutilsMimes::customizeTableWidget(ui->tableWidget_total);
}

void GestReproduction::on_pushButtonEnregistrer_clicked()
{
    QString dateColonisation;
    if(ui->comboBox_TypeRuche->currentText().isEmpty() || ui->lineEdit_numRuche->text().isEmpty()){
            QMessageBox::critical(0,"Enregistrement","Veuillez remplir tous les champs obligatoires SVP !");
    }
    else
    {
        if(ui->comboBox_TypeRuche->currentText()=="Non colonisé")
            dateColonisation=ui->dateTimeEdit_colonisation->dateTime().toString("yyyy-MM-dd h:m:s");
        else
            dateColonisation=ui->dateTimeEdit_colonisation->dateTime().toString("yyyy-MM-dd h:m:s");
        QSqlQuery reqidtypR;
        reqidtypR.exec("SELECT idTypeR FROM typeruche WHERE libelleTypeR='"+ui->comboBox_TypeRuche->currentText()+"'");

        int idTypeRuche = 0;
        if(reqidtypR.next())
            idTypeRuche = reqidtypR.value(0).toInt();

        requeteInsertion.prepare("INSERT INTO ruche(idTypeR, numRuche, dateInstallation, dateColonisation, idUser)"
                                 " VALUES "
                                 "(?,?,'"+ui->dateTimeEdit_installation->dateTime().toString("yyyy-MM-dd h:m:s")+"',"
                                 "'"+dateColonisation+"',?)");
        requeteInsertion.bindValue(0, idTypeRuche);
        requeteInsertion.bindValue(1,ui->lineEdit_numRuche->text());
        requeteInsertion.bindValue(2,idUser.toInt());


        if(!requeteInsertion.exec())
            QMessageBox::warning(this,"Erreur insertion reproduction",requeteInsertion.lastError().text());
        afficherInfo();
        effacerChamps();
       }
}

void GestReproduction::on_comboBox_critere_currentTextChanged(const QString &arg1)
{
    if(arg1=="Date d'installation" or arg1=="Date de colonisation")
   {   ui->dateEdit_debut->setDate(QDate::currentDate());
       ui->dateEdit_fin->setDate(QDate::currentDate());
       ui->label_debut->show();
       ui->dateEdit_debut->show();
       ui->label_fin->show();
       ui->dateEdit_fin->show();
       ui->comboBox_liste_Critere->hide();
       ui->label_debut->setGeometry(300,10,110,31);
       ui->dateEdit_debut->setGeometry(400,10,110,31);
       ui->label_fin->setGeometry(520,10,110,31);
       ui->dateEdit_fin->setGeometry(600,10,110,31);
   }
   else
   {
       ui->comboBox_liste_Critere->clear();
       ui->comboBox_liste_Critere->addItem("");
       ui->comboBox_liste_Critere->show();
       ui->label_debut->hide();
       ui->label_fin->hide();
       ui->dateEdit_debut->hide();
       ui->dateEdit_fin->hide();
        if(arg1=="Type de ruche")
       {
          requeteConsulter.exec("SELECT DISTINCT libelleTypeR FROM typeruche");
       }
       else if(arg1=="Numéro de la ruche")
       {
           requeteConsulter.exec("SELECT DISTINCT numRuche FROM ruche ");

       }


       modeleCritere->setQuery(requeteConsulter);
       int i = 0;
       while (requeteConsulter.next())
       {
           ui->comboBox_liste_Critere->addItem(modeleCritere->record(i).value(0).toString());
           i++;
       }
   }
}

void GestReproduction::on_comboBox_liste_Critere_currentTextChanged(const QString &arg1)
{
    QString critere_Choix;
    critere_Choix=ui->comboBox_critere->currentText();
    if(critere_Choix=="Type de ruche")
    {
        requeteConsulter.prepare("SELECT libelleTypeR, numRuche, date_format(dateInstallation,'%d/%m/%Y %H:%i:%S'), "
                                             "date_format(dateColonisation,'%d/%m/%Y %H:%i:%S'), idRuche "
                                             "FROM ruche "
                                             "INNER JOIN typeruche ON ruche.idTypeR = typeruche.idTypeR "
                                             "WHERE libelleTypeR=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Numéro de la ruche")
    {
        requeteConsulter.prepare("SELECT libelleTypeR, numRuche, date_format(dateInstallation,'%d/%m/%Y %H:%i:%S'), "
                                             "date_format(dateColonisation,'%d/%m/%Y %H:%i:%S'), idRuche "
                                             "FROM ruche "
                                             "INNER JOIN typeruche ON ruche.idTypeR = typeruche.idTypeR "
                                             "WHERE numRuche=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }

    modeleConsulter->setQuery(requeteConsulter);
     afficherInfo2();
}

void GestReproduction::on_dateEdit_debut_dateChanged(const QDate &date)
{
        QString critereDate= ui->comboBox_critere->currentText();
        ui->dateEdit_fin->setDate(QDate::currentDate());
        if (date > ui->dateEdit_fin->date()){
        QMessageBox::information(0,"Erreur de date","La date de debut ne peut pas être supérieur à la date de fin");
        ui->tableWidgetcons->setRowCount(0);
       }
        else
        {
            if(critereDate=="Date d'installation")
            {
                requeteConsulter.prepare("SELECT libelleTypeR, numRuche, date_format(dateInstallation,'%d/%m/%Y %H:%i:%S'), "
                                                     "date_format(dateColonisation,'%d/%m/%Y %H:%i:%S'), idRuche "
                                                     "FROM ruche "
                                                     "INNER JOIN typeruche ON ruche.idTypeR = typeruche.idTypeR "
                                                     "WHERE  date(dateInstallation) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                                                     "'"+ui->dateEdit_fin->date().toString("yyyy-MM-dd")+"' ");
             }

            if(critereDate=="Date de colonisation")
            {
                requeteConsulter.prepare("SELECT libelleTypeR, numRuche, date_format(dateInstallation,'%d/%m/%Y %H:%i:%S'), "
                                                     "date_format(dateColonisation,'%d/%m/%Y %H:%i:%S'), idRuche "
                                                     "FROM ruche "
                                                     "INNER JOIN typeruche ON ruche.idTypeR = typeruche.idTypeR "
                                                     "WHERE  date(dateColonisation) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                                                     "'"+ui->dateEdit_fin->date().toString("yyyy-MM-dd")+"' ");
            }
        if(!requeteConsulter.exec())
                QMessageBox::warning(0, "Erreur requete critere date", requeteConsulter.lastError().text());
        }
        modeleConsulter->setQuery(requeteConsulter);
        afficherInfo2();
}

void GestReproduction::on_dateEdit_fin_dateChanged(const QDate &date)
{
    QString critereDate= ui->comboBox_critere->currentText();
    if(date < ui->dateEdit_debut->date())
    {
          QMessageBox::information(0,"ERREUR DE DATE","La date de fin ne peut pas être inférieur à la date de début ");
          ui->tableWidgetcons->setRowCount(0);
     }

    else
    {
        if(critereDate=="Date d'installation")
        {
            requeteConsulter.prepare("SELECT libelleTypeR, numRuche, date_format(dateInstallation,'%d/%m/%Y %H:%i:%S'), "
                                                 "date_format(dateColonisation,'%d/%m/%Y %H:%i:%S'), idRuche "
                                                 "FROM ruche "
                                                 "INNER JOIN typeruche ON ruche.idTypeR = typeruche.idTypeR "
                                                 "WHERE  date(dateInstallation) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                                                 "'"+ui->dateEdit_debut->date().toString("yyyy-MM-dd")+"' ");
        }

        if(critereDate=="Date de colonisation")
        {
            requeteConsulter.prepare("SELECT libelleTypeR, numRuche, date_format(dateInstallation,'%d/%m/%Y %H:%i:%S'), "
                                                 "date_format(dateColonisation,'%d/%m/%Y %H:%i:%S'), idRuche "
                                                 "FROM ruche "
                                                 "INNER JOIN typeruche ON ruche.idTypeR = typeruche.idTypeR "
                                                 "WHERE  date(dateColonisation) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                                                 "'"+ui->dateEdit_debut->date().toString("yyyy-MM-dd")+"' ");
         }

        if(!requeteConsulter.exec())
            QMessageBox::warning(0, "Erreur requete critere date", requeteConsulter.lastError().text());
     }
      modeleConsulter->setQuery(requeteConsulter);
      afficherInfo2();
}

void GestReproduction::on_tableWidget_enregistrment_doubleClicked(const QModelIndex &index)
{
    int ligne = index.row();
    ui->comboBox_TypeRuche->setCurrentText(modeleSelection->record(ligne).value(0).toString());
    ui->lineEdit_numRuche->setText(modeleSelection->record(ligne).value(1).toString());
    ui->dateTimeEdit_installation->setDateTime(modeleSelection->record(ligne).value(2).toDateTime());
    ui->dateTimeEdit_colonisation->setDateTime(modeleSelection->record(ligne).value(3).toDateTime());

    numero=modeleSelection->record(ligne).value(4).toInt();
    ui->pushButtonModifier->setEnabled(true);
    ui->pushButtonEnregistrer->setDisabled(false);
}

void GestReproduction::on_tableWidgetcons_doubleClicked(const QModelIndex &index)
{
    ui->tabWidget->setCurrentIndex(0);
    int ligne = index.row();
    ui->comboBox_TypeRuche->setCurrentText(modeleConsulter->record(ligne).value(0).toString());
    ui->lineEdit_numRuche->setText(modeleConsulter->record(ligne).value(1).toString());
    ui->dateTimeEdit_installation->setDateTime(modeleConsulter->record(ligne).value(2).toDateTime());
    ui->dateTimeEdit_colonisation->setDateTime(modeleConsulter->record(ligne).value(3).toDateTime());

    numero=modeleConsulter->record(ligne).value(4).toInt();
    ui->pushButtonModifier->setEnabled(true);
    ui->pushButtonEnregistrer->setDisabled(true);
}

void GestReproduction::on_pushButtonModifier_clicked()
{
    if(ui->comboBox_TypeRuche->currentText().isEmpty() || ui->lineEdit_numRuche->text().isEmpty()){
            QMessageBox::critical(0,"Modification","Veuillez remplir tous les champs obligatoires SVP !");
    }
    else
    {
        QSqlQuery reqidtypR;
        reqidtypR.exec("SELECT idTypeR FROM typeruche WHERE libelleTypeR='"+ui->comboBox_TypeRuche->currentText()+"'");

        int idTypeRuche = 0;
        if(reqidtypR.next())
            idTypeRuche = reqidtypR.value(0).toInt();

        requeteInsertion.prepare("INSERT INTO ruche(idTypeR, numRuche, dateInstallation, dateColonisation, idUser)"
                                 " VALUES "
                                 "(?,?,'"+ui->dateTimeEdit_installation->dateTime().toString("yyyy-MM-dd h:m:s")+"',"
                                 "'"+ui->dateTimeEdit_colonisation->dateTime().toString("yyyy-MM-dd h:m:s")+"',?)");
        requeteInsertion.bindValue(0, idTypeRuche);
        requeteInsertion.bindValue(1,ui->lineEdit_numRuche->text());
        requeteInsertion.bindValue(2,idUser.toInt());

        requeteInsertion.prepare("UPDATE ruche SET idTypeR=?, numRuche=?,  dateInstallation='"+ui->dateTimeEdit_installation->dateTime().toString("yyyy-MM-dd h:m:s")+"',"
                                         " dateColonisation='"+ui->dateTimeEdit_colonisation->dateTime().toString("yyyy-MM-dd h:m:s")+"', "

                                         "idUser=? WHERE idRuche ='"+QString::number(numero)+"'");
        requeteInsertion.bindValue(0,idTypeRuche);
        requeteInsertion.bindValue(1,ui->lineEdit_numRuche->text());
        requeteInsertion.bindValue(2,idUser.toInt());
        if(!requeteInsertion.exec())
            QMessageBox::warning(this,"Erreur modification reproduction",requeteInsertion.lastError().text());
        afficherInfo();
        effacerChamps();

     }
}

void GestReproduction::on_dateTimeEdit_installation_dateTimeChanged(const QDateTime &dateTime)
{
    ui->dateTimeEdit_colonisation->setDateTime(dateTime.addDays(21));
    ui->dateTimeEdit_miseBas->setDateTime(dateTime.addDays(114));
}

void GestReproduction::on_pushButtonImprimerConsultation_clicked()
{
    QPrinter* printer = new  QPrinter();
    printer->setOrientation(QPrinter::Landscape);
    printer->setPaperSize(QPrinter::A4);
    QPrintPreviewDialog* apercuDialog = new QPrintPreviewDialog(printer,this);
    apercuDialog->setGeometry(10,40,1366,705);
    connect(apercuDialog, SIGNAL(paintRequested(QPrinter*)), this, SLOT(afficherApercuListe(QPrinter*)));
    apercuDialog->exec();
}

void GestReproduction::afficherApercuListe(QPrinter *printer)
{
  QString titre="Liste des reproductions";
  if(ui->comboBox_critere->currentText()=="Période")
      titre+=" du "+ui->dateEdit_debut->date().toString("dd/MM/yyyy")+" au "+ui->dateEdit_fin->date().toString("dd/MM/yyyy");
  else if(ui->comboBox_critere->currentText()=="Date enregistrement")
      titre+=" du "+ui->dateEdit_debut->date().toString("dd/MM/yyyy");
  else if(ui->comboBox_critere->currentText()=="Numéro truie")
      titre+=" de la truie n° "+ui->comboBox_liste_Critere->currentText();
  /*else if(ui->comboBox_critere->currentText()=="Batiment")
      titre+=" du batiment "+ui->comboBox_liste_Critere->currentText();
  else if (ui->comboBox_critere->currentText()=="Service de traitement")
      titre+=" traités par le service : "+ui->comboBox_liste_Critere->currentText();
  else if(ui->comboBox_critere->currentText()=="Destinataire")
      titre+=" addressé au "+ui->comboBox_liste_Critere->currentText();
  else if(ui->comboBox_critere->currentText()=="Nombre de pièces jointes")
      titre+=" ayant"+ui->comboBox_liste_Critere->currentText()+" pièces jointes";
  else if(ui->comboBox_critere->currentText()=="Réference")
      titre+=" de  référence "+ui->comboBox_liste_Critere->currentText();
  else if(ui->comboBox_critere->currentText()=="Nature")
      titre+=" de nature "+ui->comboBox_liste_Critere->currentText();*/
 // OutilsMimes::afficherApercuImpression(modelImpression, titre,"", printer);

  QString basPage = "\n\t\t\t\tTotal portée: "+ui->tableWidget_total->item(0,0)->text()+"\t\t"+
                      "Total aliment: "+ui->tableWidget_total->item(0,1)->text();
  OutilsMimes::afficherApercuImpression(ui->tableWidgetcons,titre,basPage,printer,"");
}

void GestReproduction::on_comboBox_TypeRuche_currentTextChanged(const QString &arg1)
{
    if(arg1=="Non colonisé"){
        ui->dateTimeEdit_colonisation->close();
        ui->dateTimeEdit_colonisation->setDisabled(true);
    }
    else
        ui->dateTimeEdit_colonisation->setDisabled(false);
}

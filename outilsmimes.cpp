#include "outilsmimes.h"


/*class OPLComboBox : public QComboBox
{
    Q_OBJECT

  public:
      explicit OPLComboBox(){}
      //~OPLComboBox(){}
  private slots:
      void changer_ListeFilieres(QString cycle)
      {
       QSqlQueryModel *model = new QSqlQueryModel;
       model->setQuery("SELECT Libelle FROM filiere JOIN cycle "
                       "ON cycle.idCycle = filiere.idCycle WHERE libelleCycle = '"+cycle+"';");
       //this->setModel(model);
       QMessageBox::information(0, "Liste filières", model->query().lastQuery());
      }

      void changer_ListeAnnees(QString cycle)
      {
       if(cycle == "BTS" || cycle == "Master" || cycle == "Doctorat"){
          this->addItem("1ère année");
          this->addItem("2ème année");
         }
       else if(cycle == "Licence"){
               this->addItem("1ère année");
               this->addItem("2ème année");
               this->addItem("3ème année");
            }
      }
};*/

OutilsMimes::OutilsMimes()
{
 OPLComboBox* combo = new OPLComboBox();
}

void OutilsMimes::customizeTableWidget(QTableWidget *twidget)
{
    twidget->resizeRowsToContents();
    twidget->resizeColumnsToContents();
    twidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    twidget->setAlternatingRowColors(true);
    /*QPalette palcol,couleurEntete;
    palcol.setColor(QPalette::AlternateBase,QColor(0,255,0));
    twidget->setPalette(palcol);
    couleurEntete.setColor(QPalette::Background,QColor(0, 255, 0));
    twidget->setPalette(couleurEntete);*/
    QPalette palcol;
    palcol.setColor(QPalette::AlternateBase,QColor(230,130,5));
    twidget->setPalette(palcol);
}

void OutilsMimes::customizeTableWidget2(QTableWidget *twidget)
{
    twidget->resizeRowsToContents();
    twidget->resizeColumnsToContents();
    twidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    /*twidget->setAlternatingRowColors(true);
    QPalette palcol;
    palcol.setColor(QPalette::AlternateBase,QColor(253,200,0));
    twidget->setPalette(palcol);*/
}

void OutilsMimes::customizeTableView(QTableView* tview)
{
    tview->resizeRowsToContents();
    tview->resizeColumnsToContents();
    tview->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    //tview->resizeColumnsToContents();

    //tview->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    tview->verticalHeader()->setSectionsClickable(true);
    tview->horizontalHeader()->setSectionsClickable(false);
    tview->setAlternatingRowColors(true);
    QPalette palcol;
    palcol.setColor(QPalette::AlternateBase,QColor(216, 230, 236));
    tview->setPalette(palcol);
}

void OutilsMimes::customComboBox(QComboBox* cbox, QSqlQuery query)
{
    cbox->clear();
    QSqlQueryModel *mod = new QSqlQueryModel;
    query.exec();
    mod->setQuery(query);
    cbox->setEditable(true);
    cbox->addItem("");
    //cbox->setModel(mod);
    for(int i=0;i<query.size();i++)
            cbox->addItem(mod->record(i).value(0).toString());
    QCompleter *comp = new QCompleter(mod);
    comp->setCaseSensitivity(Qt::CaseInsensitive);
    cbox->setCompleter(comp);
    //cbox->clearEditText();
}

void OutilsMimes::customLineEdit(QLineEdit* lineEdit, QSqlQuery query)
{
    QSqlQueryModel *mod = new QSqlQueryModel;
    query.exec();
    mod->setQuery(query);
    QCompleter *comp = new QCompleter(mod);
    comp->setCaseSensitivity(Qt::CaseInsensitive);
    lineEdit->setCompleter(comp);
}

void OutilsMimes::customizeToolButton(QToolButton* toolButton, QString textButton, QString iconButton)
{
    toolButton->setText(textButton);
    toolButton->setIcon(QIcon(iconButton));
    toolButton->setPopupMode(QToolButton::InstantPopup);
    toolButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolButton->setFont(QFont("Calibri", 12, 6));
}

QDate OutilsMimes::stringToDate(QString dateString)
{
    QString strJour, strMois, strAnnee;
    strJour.append(dateString.at(0));
    strJour.append(dateString.at(1));
    int jour = strJour.toInt();
    strMois.append(dateString.at(3));
    strMois.append(dateString.at(4));
    int mois = strMois.toInt();
    strAnnee.append(dateString.at(6));
    strAnnee.append(dateString.at(7));
    strAnnee.append(dateString.at(8));
    strAnnee.append(dateString.at(9));
    int annee = strAnnee.toInt();
    QDate date;
    date.setDate(annee, mois, jour);
    return date;
}

void OutilsMimes::afficherApercuImpression(QSqlQueryModel* model, QString titreTable, QString piedPage, QPrinter *printer ){

    QTextBrowser * editor = new QTextBrowser();
    /*QString pays = "République du Bénin";
    QString ministere = "Ministère de l'Enseignement Supérieur et de la Recherche Scientifique";
    QString universite = "Université d'Abomey-Calavi";
    QString entiteUniversitaire = "Ecole Nationale d'Economie Appliquée et de Management (ENEAM)";*/
    /*editor->append("<html><head><meta charset = \"utf-8\" /><style>"
         ".entete{text-align:center; font: 12pt \"Arial\"; color: black}"
         ".ligne1{text-align:center; font: 12pt \"Arial\"; color: red}"
         ".ligne2{text-align:center; font: 12pt \"Arial\"; color: blue}"
         ".imageflottante{float: left;}</style></head><body>"
         "<p><img src= \":/icones/logoCesmar2.png\" class=\"imageflottante\" /></p><p>  </p>"
         "<div  style =\"text-align:center;\"><p class = \"entete\" >"+pays+"</p>"
         "<p>****</p><p class = \"entete\" >"+ministere+"</p><p>****</p>"
         "<p class = \"entete\" >"+universite+"</p><p>****</p>"
         "<p class = \"entete\" >"+entiteUniversitaire+"</p></div><br /></body></html> ");*/
    QTextCharFormat enteteFormat;
    enteteFormat.setFont(QFont("Times New Roman", 12));
    editor->setCurrentCharFormat(enteteFormat);
    editor->setAlignment(Qt::AlignHCenter);
    /*editor->append(pays+"\n****\n"+ministere+"\n****\n"+universite
                   +"\n****\n"+entiteUniversitaire+"\n");*/
    QFont police("Times New Roman");
    editor->setCurrentFont(police);
    QTextCharFormat NormalFormat;
    editor->setCurrentCharFormat(NormalFormat);
    editor->setAlignment(Qt::AlignCenter);
    editor->append("\n");
    //on insere le titre du tableau
    QTextCharFormat format_titre;
    format_titre.setFont(QFont("Times New Roman", 14, QFont::ExtraLight));
    editor->setCurrentCharFormat(format_titre);
    editor->append(titreTable+"\n");

    //on crée un curseur a l'endroit du curseur actuel
    QTextCursor cursor = editor->textCursor();
    cursor.beginEditBlock();

    //Creation du format du tableau qui sera imprimer
    QTextTableFormat tableFormat;
    tableFormat.setBackground(QColor("#ffffff"));
    tableFormat.setWidth(1000);
    tableFormat.setCellPadding(5);
    tableFormat.setCellSpacing(0);
    tableFormat.setBorder(0.5);
    tableFormat.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
    tableFormat.setBorderBrush(QBrush(Qt::black,Qt::SolidPattern));
    tableFormat.setAlignment(Qt::AlignHCenter);

    //Creation du tableau qui sera imprimé
    QTextTable * tableau = cursor.insertTable(model->rowCount()+1, model->columnCount()+1, tableFormat);

    //Format des entêtes du tableau
    QTextCharFormat format_entete_tableau;
    format_entete_tableau.setFont(QFont("Times New Roman", 12, QFont::ExtraLight));
    format_entete_tableau.setVerticalAlignment(QTextCharFormat::AlignMiddle);

    //Format du texte des cellules du tableau
    QTextCharFormat format_cellule;
    format_cellule.setFont(QFont("Times New Roman", 12));

    //on ecrit les HEADERS du tableaux dans le tableau a imprimer
    //on selectionne la premiere cellule de la première colonne
    QTextTableCell cellule = tableau->cellAt(0,0);

    //on place le curseur a cet endroit
    QTextCursor cellCursor = cellule.firstCursorPosition();
    cellCursor.insertText("N°",format_entete_tableau);

    for ( int i = 0 ; i < model->columnCount() ; i++ )
    {
        //on selectionne la premiere cellule de chaque colonne
        cellule = tableau->cellAt(0, i+1);
        //on place le curseur a cet endroit
        cellCursor = cellule.firstCursorPosition();
        //on écrit dans la cellule
        cellCursor.insertText(model->headerData(i, Qt::Horizontal).toString(),format_entete_tableau);
    }

    //Remplissage de la colonne des numéros d'ordre

 for (int j=1; j <= model->rowCount(); j++){
  cellule = tableau->cellAt(j,0);
  cellCursor = cellule.firstCursorPosition();
  cellCursor.insertText(QString::number(j),format_cellule);
 }
 //Remplissage des autres cellules
    for (int row = 1; row < tableau->rows(); row ++)
    {
        for (int col = 1; col < tableau->columns(); col ++)
        {
          cellule = tableau->cellAt(row,col);
          cellCursor = cellule.firstCursorPosition();
          cellCursor.insertText(model->record(row-1).value(col-1).toString(),format_cellule);
          //On a row-1 car le tableau a une ligne de plus que le modèle
        }
    }
    //int lastColumn = tableau->columns()-1;
    tableau->removeColumns(1,1);
    tableau->removeColumns(4, 1);
    tableau->removeColumns(8, 2);
    editor->setCurrentCharFormat(NormalFormat);
    editor->append("\n");
    editor->setAlignment(Qt::AlignHCenter);
    editor->append(piedPage);
    //fin de l'edition
    cursor.endEditBlock();
    //impression de notre editor dans le QPrinter
    editor->print(printer);
}

void OutilsMimes::afficherApercuImpression(QTableWidget* tableWidget, QString titreTable, QString piedPage, QPrinter *printer,QString signataire )
{

   // QString pays = "République du Bénin";
    //QString ministere = "Ministère de l'Enseignement Supérieur et de la Recherche Scientifique";
    //QString universite = "";
    QString entiteUniversitaire = "Ferme KDK";
    QTextBrowser * editor = new QTextBrowser();
    /*editor->append("<html><head><meta charset = \"utf-8\" /><style>"
         ".entete{text-align:center; font: 12pt \"Arial\"; color: black}"
         ".ligne1{text-align:center; font: 12pt \"Arial\"; color: red}"
         ".ligne2{text-align:center; font: 12pt \"Arial\"; color: blue}"
         ".imageflottante{float: left;}</style></head><body>"
         "<p><img src= \":/icones/logoCesmar2.png\" class=\"imageflottante\" /></p><p>  </p>"
         "<div  style =\"text-align:center;\"><p class = \"entete\" >"+pays+"</p>"
         "<p>****</p><p class = \"entete\" >"+ministere+"</p><p>****</p>"
         "<p class = \"entete\" >"+universite+"</p><p>****</p>"
         "<p class = \"entete\" >"+entiteUniversitaire+"</p></div><br /></body></html> ");*/
    QTextCharFormat enteteFormat;
    enteteFormat.setFont(QFont("Times New Roman", 18));
    editor->setCurrentCharFormat(enteteFormat);
    editor->setAlignment(Qt::AlignHCenter);
    /*editor->append(pays+"\n****\n"+ministere+"\n****\n"+universite
                   +"\n****\n"+entiteUniversitaire+"\n");*/
   // editor->append(universite+"\n****\n"+entiteUniversitaire+"\n****");
    editor->append(entiteUniversitaire+"\n****");
    QFont police("Times New Roman");
    editor->setCurrentFont(police);
    QTextCharFormat NormalFormat;
    editor->setCurrentCharFormat(NormalFormat);
    editor->setAlignment(Qt::AlignCenter);
    //editor->append("\n");
    //on insere le titre du tableau
    QTextCharFormat format_titre;
    format_titre.setFont(QFont("Times New Roman", 14, QFont::ExtraLight));
    editor->setCurrentCharFormat(format_titre);
    editor->append(titreTable);

    //on crée un curseur a l'endroit du curseur actuel
    QTextCursor cursor = editor->textCursor();
    cursor.beginEditBlock();

    //Creation du format du tableau qui sera imprimé
    QTextTableFormat tableFormat;
    tableFormat.setBackground(QColor("#ffffff"));
    tableFormat.setWidth(1000);
    tableFormat.setCellPadding(4);
    tableFormat.setCellSpacing(0);
    tableFormat.setBorder(0.2);
    tableFormat.setBorderBrush(QBrush(QColor("black"), Qt::SolidPattern));
    tableFormat.setAlignment(Qt::AlignHCenter);

    //Creation du tableau qui sera imprimé avec le nombre de colonne
    //et de ligne que contient le tableau mis en parametre
    //en enlevant la colonne des liens.
    QTextTable * tableau = cursor.insertTable(tableWidget->rowCount()+1, tableWidget->columnCount()+1, tableFormat);

    //Format des entêtes du tableau
    QTextCharFormat format_entete_tableau;
    format_entete_tableau.setFont(QFont("Times New Roman", 12, QFont::ExtraLight));
    format_entete_tableau.setVerticalAlignment(QTextCharFormat::AlignMiddle);

    //Format du texte des cellules du tableau
    QTextCharFormat format_cellule;
    format_cellule.setFont(QFont("Times New Roman", 12, QFont::ExtraLight));

    //on ecrit les HEADERS du tableaux dans le tableau a imprimer
    //on selectionne la premiere cellule de la première colonne
    QTextTableCell cellule = tableau->cellAt(0,0);

    //on place le curseur a cet endroit
    QTextCursor cellCursor = cellule.firstCursorPosition();
    cellCursor.insertText("N°", format_entete_tableau);

    for ( int i = 0 ; i < tableWidget->columnCount() ; i++ )
    {
        //on selectionne la premiere cellule de chaque colonne
        cellule = tableau->cellAt(0, i+1);
        //on place le curseur a cet endroit
        cellCursor = cellule.firstCursorPosition();
        //on écrit dans la cellule
        cellCursor.insertText(tableWidget->horizontalHeaderItem(i)->text(),format_entete_tableau);
    }

    //Remplissage de la colonne des numéros d'ordre

 for (int j=1; j <= tableWidget->rowCount(); j++){
  cellule = tableau->cellAt(j,0);
  cellCursor = cellule.firstCursorPosition();
  cellCursor.insertText(QString::number(j),format_cellule);
 }

    for (int row = 1; row < tableau->rows(); row ++)
    {
        for (int col = 1; col < tableau->columns(); col ++)
        {
          cellule = tableau->cellAt(row,col);
          cellCursor = cellule.firstCursorPosition();
          cellCursor.insertText(tableWidget->item(row-1,col-1)->text(),format_cellule);
          //On a row-1 et col-1 car le tableau a une ligne et une colonne de plus que le tableWiget
        }

    }
    editor->setCurrentCharFormat(NormalFormat);
    editor->setCurrentCharFormat(format_titre);
    editor->append("\n\t\t\t\t"+piedPage);
    /*editor->append("\n\t\t\t\t"+piedPage+"\n\n\t\t\t\t Le Directeur Adjoint,");
    editor->append("<html><p style = \"font-family: Times New Roman; font-size: 14pt\">"
                   "<br /><br /><br /><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "<u><strong>"+signataire+"</strong></u></html>");
    editor->append("<html><p style = \"font-family: Times New Roman; font-size: 14pt\">"
                   "<br /><br /><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                   "<i>© MIMES</i></html>");*/

    //fin de l'edition
    cursor.endEditBlock();
    //impression de notre editor dans le QPrinter
    editor->print(printer);
}

void OutilsMimes::creerFichierExcel(QSqlQueryModel *model, QString titreTable, QString piedPage)
{
    QXlsx::Document docExcel; //Document excel au format docExcel
    QXlsx::Format format_GrandEntete;
    format_GrandEntete.setFont(QFont("Arial", 9, QFont::DemiBold));
    format_GrandEntete.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    format_GrandEntete.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    QXlsx::Format formatEntete;
    formatEntete.setBorderStyle(QXlsx::Format::BorderMedium);
    formatEntete.setBorderColor(QColor(Qt::black));
    formatEntete.setFont(QFont("Arial", 8, QFont::DemiBold));
    formatEntete.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatEntete.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    QXlsx::Format formatCellule;
    formatCellule.setBorderStyle(QXlsx::Format::BorderMedium);
    formatCellule.setBorderColor(QColor(Qt::black));
    formatCellule.setFont(QFont("Arial", 8));
    formatCellule.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatCellule.setVerticalAlignment(QXlsx::Format::AlignVCenter);
 //Ecriture de l'entête principal
    docExcel.mergeCells(QXlsx::CellRange(1, 1, 1, 3));
    docExcel.write(1, 1, "", format_GrandEntete);
    docExcel.mergeCells(QXlsx::CellRange(2, 1, 2, 3));
    docExcel.write(2, 1, "*****", format_GrandEntete);
    docExcel.mergeCells(QXlsx::CellRange(3, 1, 3, 3));
    docExcel.write(3, 1, "Ferme KDK", format_GrandEntete);

 //Ecriture du titre du tableau
    docExcel.mergeCells(QXlsx::CellRange(5, 1, 5, 5));
    docExcel.write(5, 1, titreTable, format_GrandEntete);
//Ecriture de l'entête du tableau
    docExcel.write(7, 1, "N°", formatEntete);
    for(int col=0; col < model->columnCount()-1; col++)
    {
        if(col==0)
            docExcel.write(7, col+2, "Bande", formatEntete);
        else
            docExcel.write(7, col+2, model->headerData(col, Qt::Horizontal).toString(), formatEntete);
    }
//Ecriture du corps du tableau
    for(int row=0; row < model->rowCount(); row++)
    {
     //Ecriture du numéro d'ordre
     docExcel.write(row+8, 1, QString::number(row+1), formatCellule);
     for(int col=0; col < model->columnCount()-1; col++)
     {
         if(col==0)
            docExcel.write(row+8, col+2, "Bande du \n"+model->record(row).value(col).toString(), formatCellule);
         else
            docExcel.write(row+8, col+2, model->record(row).value(col).toString(), formatCellule);
     }
    }

    //Ecriture du pied de la page
    int row = model->rowCount()+9;
    docExcel.write(row, 1, piedPage, format_GrandEntete);
    //Enregistrement du fichier
    QString nomFichier = titreTable;
    nomFichier.replace(":", "_");
    nomFichier.replace("/", "_");
    QString cheminFichier = QFileDialog::getSaveFileName(0, "Enregistrer le fichier",
                          QStandardPaths::DocumentsLocation+"/"+nomFichier, "*.xlsx");

    docExcel.saveAs(cheminFichier);
    QStringList fichier;
            fichier << cheminFichier;
    QProcess::startDetached("C:\\Program Files\\Microsoft Office\\Office15\\EXCEL.exe", fichier);
}

#ifndef GESTREPRODUCTION_H
#define GESTREPRODUCTION_H

#include <QWidget>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QTableWidget>
#include "outilsmimes.h"
#include <QMenu>

namespace Ui {
class GestReproduction;
}

class GestReproduction : public QWidget
{
    Q_OBJECT

public:
    explicit GestReproduction(QWidget *parent = 0, QString currentUser="");
    ~GestReproduction();
    void changerOgletActif(int ind);

private slots:
    void on_pushButtonEnregistrer_clicked();
    void afficherInfo();
    void effacerChamps();
    void afficherInfo2();
    /*void Menucontextuel(QPoint position);
    void Menucontextuel2(QPoint position);
    void supprimerConsultation();
    void supprimerConsultation2();
    void afficherApercuListe(QPrinter *printer);*/

    void on_comboBox_critere_currentTextChanged(const QString &arg1);

    void on_comboBox_liste_Critere_currentTextChanged(const QString &arg1);

    void on_dateEdit_debut_dateChanged(const QDate &date);

    void on_dateEdit_fin_dateChanged(const QDate &date);

    void on_tableWidget_enregistrment_doubleClicked(const QModelIndex &index);

    void on_tableWidgetcons_doubleClicked(const QModelIndex &index);

    void on_pushButtonModifier_clicked();
    void affich();

    void on_dateTimeEdit_installation_dateTimeChanged(const QDateTime &dateTime);

    void on_pushButtonImprimerConsultation_clicked();
    void afficherApercuListe(QPrinter *printer);

    void on_comboBox_TypeRuche_currentTextChanged(const QString &arg1);

private:
    Ui::GestReproduction *ui;
    QSqlQuery requeteSelection,requeteInsertion,requeteConsulter,requeteAfficher,reqEXP,reqObj,reqService,
                reqNature,reqPerson,reqUser,requeteRecuperation,Reponse;
    QSqlQueryModel *modeleSelection,*modeleConsulter,*modeleCritere,*modEXP,*modObj,*modService,*modNature,
                    *modPerson,*modeleProfil,*modeleRecuperation;
    int numero;
    QString idUser;
    QMenu *menuContext,*menuContext2;
    QAction *action_supprimer,*action_supprimer2;
};

#endif // GESTREPRODUCTION_H

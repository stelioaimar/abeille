#include "livraison.h"
#include "ui_livraison.h"

livraison::livraison(QWidget *parent, QString currentUser) :
    QWidget(parent),
    ui(new Ui::livraison)
{
    ui->setupUi(this);
    idUser=currentUser;
    ui->dateEdit_Livraison->setDate(QDate::currentDate());
    //ui->dateEdit_Enreg->setDate(QDate::currentDate());
    //ui->dateEdit_Enreg->hide();
    ui->dateEdit_debut->hide();
    ui->dateEdit_fin->hide();
    ui->label_debut->hide();
    ui->label_fin->hide();
    //ui->dateEdit_debut->setDate(QDate::currentDate());
   //ui->dateEdit_fin->setDate(QDate::currentDate());

    modeleSelection = new QSqlQueryModel();
    modeleCritere = new QSqlQueryModel();
    modelConsulter = new QSqlQueryModel();
    modeleInit = new QSqlQueryModel();
    modeleEntree = new QSqlQueryModel();
    afficherInfo();
    afficherInfo2();
    affich();



}

livraison::~livraison()
{
    delete ui;
}
void livraison::changerOgletActif(int ind)
{
    ui->tabWidget->setCurrentIndex(ind);
}

void livraison::on_pushButtonEnregistrer_clicked()
{

    reqinit.exec("SELECT pltMagasin, unitMagasin from livraison  order by idLivraison desc limit 1");
    reqentre.exec("SELECT nbPlOeuf, nbUnitOeuf from ramassage  WHERE dateRamassage= '"+QDate::currentDate().toString("yyyy-MM-dd")+"' ");
    modeleInit->setQuery(reqinit);
    modeleEntree->setQuery(reqentre);
    if(!reqinit.exec())
        QMessageBox::warning(this,"Erreur insertion livraison",reqinit.lastError().text());
    if(!reqentre.exec())
        QMessageBox::warning(this,"Erreur insertion livraison",reqentre.lastError().text());


    if(ui->lineEdit->text().isEmpty()){
            QMessageBox::critical(0,"Enregistrement","Veuillez remplir tous les champs obligatoires SVP !");
    }else {
    requeteInsertion.prepare("INSERT INTO livraison(dateLivraison, pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                             "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                             " observation, dateEnregistrement, idUser) VALUES "
                               "('"+ui->dateEdit_Livraison->date().toString("yyyy-MM-dd")+"',?,?,?,?,?,?,?,?,?,?,?,?,"
                               "'"+QDate::currentDate().toString("yyyy-MM-dd")+"',?)");

      requeteInsertion.bindValue(0,modeleInit->record(0).value(0).toInt());
      requeteInsertion.bindValue(1,modeleInit->record(0).value(1).toInt());
      requeteInsertion.bindValue(2,modeleEntree->record(0).value(0).toInt());
      requeteInsertion.bindValue(3,modeleEntree->record(0).value(1).toInt());//ddd
      //int val=200;
      //int result=ui->spinBox_Entree->value()-(ui->spinBox_pcl->value()+ui->spinBox_pnl->value());
      int resultat=0,unitRestant=0;
      if ((modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())>=30)
      {
          resultat = (modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())/30;
          unitRestant=(modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())-(resultat*30);
      }
      else
          unitRestant=(modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt());
      requeteInsertion.bindValue(4,modeleInit->record(0).value(0).toInt()+ modeleEntree->record(0).value(0).toInt()+resultat);
      requeteInsertion.bindValue(5,unitRestant);
      requeteInsertion.bindValue(6,ui->spinBox_pltNorLivre->value());
      requeteInsertion.bindValue(7,ui->spinBox_pltCasseLivre->value());
      requeteInsertion.bindValue(8,(modeleInit->record(0).value(0).toInt()+ modeleEntree->record(0).value(0).toInt()+resultat)-(ui->spinBox_pltNorLivre->value()+ui->spinBox_pltCasseLivre->value()));
      requeteInsertion.bindValue(9,unitRestant);
      requeteInsertion.bindValue(10,ui->lineEdit->text());
      requeteInsertion.bindValue(11,ui->textEdit_observation->toPlainText());
      requeteInsertion.bindValue(12,idUser.toInt());

      if(!requeteInsertion.exec())
          QMessageBox::warning(this,"Erreur insertion livraison",requeteInsertion.lastError().text());
      afficherInfo();
      effacerChamps();
      }
}

void livraison::afficherInfo()
{
    requeteSelection.prepare("SELECT date_format(dateLivraison,'%d/%m/%Y'),pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                             "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                             " observation, idLivraison FROM "
                             "livraison WHERE date(dateLivraison) ='"+QDate::currentDate().toString("yyyy-MM-dd")+"'");
         if (!requeteSelection.exec())
        QMessageBox::information(this,"Erreur selection livraison",requeteSelection.lastError().text());
        modeleSelection->setQuery(requeteSelection);
        ui->tableWidget_Enreg->setColumnCount(13);
        QStringList entete;
        entete<<"DATE DE\nLIVRAISON"<<"PLATEAUX\nINITIAL"<<"UNITES\nINITIAL"<<"PLATEAUX\nENTRES"<<"UNITES\nENTRES"<<"PLATEAUX\nDISPONIBLES"<<"UNITES\nDISPONIBLES"
         <<"PLATEAUX\nNORMALS\nLIVRES"<<"PLATEAUX\nCASSES\nLIVRES"<<"PLATEAUX\nEN\nMAGASIN"<<"UNITES EN\nMAGASIN"<<"RECEVEUR"<<"OBSERV\nATION";
        ui->tableWidget_Enreg->setHorizontalHeaderLabels(entete);
         for (int i=0; i<requeteSelection.size(); i++)
         {
             ui->tableWidget_Enreg->setRowCount(i+1);
             QTableWidgetItem *itemDateLivraison = new QTableWidgetItem(modeleSelection->record(i).value(0).toString());
             itemDateLivraison->setFlags(Qt::ItemIsEnabled);
             itemDateLivraison->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itempltInitiale = new QTableWidgetItem(modeleSelection->record(i).value(1).toString());
             itempltInitiale->setFlags(Qt::ItemIsEnabled);
             itempltInitiale->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itemuniteInitiale = new QTableWidgetItem(modeleSelection->record(i).value(2).toString());
             itemuniteInitiale->setFlags(Qt::ItemIsEnabled);
             itemuniteInitiale->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itempltEntree = new QTableWidgetItem(modeleSelection->record(i).value(3).toString());
             itempltEntree->setFlags(Qt::ItemIsEnabled);
             itempltEntree->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itemuniteEntree = new QTableWidgetItem(modeleSelection->record(i).value(4).toString());
             itemuniteEntree->setFlags(Qt::ItemIsEnabled);
             itemuniteEntree->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itempltDisponible = new QTableWidgetItem(modeleSelection->record(i).value(5).toString());
             itempltDisponible->setFlags(Qt::ItemIsEnabled);
             itempltDisponible->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itemuniteDisponiible = new QTableWidgetItem(modeleSelection->record(i).value(6).toString());
             itemuniteDisponiible->setFlags(Qt::ItemIsEnabled);
             itemuniteDisponiible->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itemnbPltNormLivre = new QTableWidgetItem(modeleSelection->record(i).value(7).toString());
             itemnbPltNormLivre->setFlags(Qt::ItemIsEnabled);
             itemnbPltNormLivre->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itemnbPltCasseLivre = new QTableWidgetItem(modeleSelection->record(i).value(8).toString());
             itemnbPltCasseLivre->setFlags(Qt::ItemIsEnabled);
             itemnbPltCasseLivre->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itempltMagasin = new QTableWidgetItem(modeleSelection->record(i).value(9).toString());
             itempltMagasin->setFlags(Qt::ItemIsEnabled);
             itempltMagasin->setTextAlignment(Qt::AlignCenter);

             QTableWidgetItem *itemunitMagasin = new QTableWidgetItem(modeleSelection->record(i).value(10).toString());
             itemunitMagasin->setFlags(Qt::ItemIsEnabled);
             itemunitMagasin->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itemreceveur = new QTableWidgetItem(modeleSelection->record(i).value(11).toString());
             itemreceveur->setFlags(Qt::ItemIsEnabled);
             itemreceveur->setTextAlignment(Qt::AlignCenter);
             QTableWidgetItem *itemobservation = new QTableWidgetItem(modeleSelection->record(i).value(12).toString());
             itemobservation->setFlags(Qt::ItemIsEnabled);
             itemobservation->setTextAlignment(Qt::AlignCenter);

             ui->tableWidget_Enreg->setItem(i,0,itemDateLivraison);
             ui->tableWidget_Enreg->setItem(i,1,itempltInitiale);
             ui->tableWidget_Enreg->setItem(i,2,itemuniteInitiale);
             ui->tableWidget_Enreg->setItem(i,3,itempltEntree);
             ui->tableWidget_Enreg->setItem(i,4,itemuniteEntree);
             ui->tableWidget_Enreg->setItem(i,5,itempltDisponible);
             ui->tableWidget_Enreg->setItem(i,6,itemuniteDisponiible);
             ui->tableWidget_Enreg->setItem(i,7,itemnbPltNormLivre);
             ui->tableWidget_Enreg->setItem(i,8,itemnbPltCasseLivre);
             ui->tableWidget_Enreg->setItem(i,9,itempltMagasin);
             ui->tableWidget_Enreg->setItem(i,10,itemunitMagasin);
             ui->tableWidget_Enreg->setItem(i,11,itemreceveur);
             ui->tableWidget_Enreg->setItem(i,12,itemobservation);
         }
         OutilsMimes::customizeTableWidget(ui->tableWidget_Enreg);
         ui->pushButtonEnregistrer->setEnabled(true);
         ui->pushButtonModifier->setDisabled(true);
}

void livraison::effacerChamps()
{
    ui->spinBox_pltCasseLivre->clear();
    ui->spinBox_pltNorLivre->clear();
    //ui->spinBox_uom->clear();
    //ui->spinBox_pom->clear();
    //ui->spinBox_Entree->clear();
    //ui->spinBox_pcl->clear();
    ui->lineEdit->clear();
    //ui->spinBox_pcl->clear();
    ui->textEdit_observation->clear();
}

void livraison::on_tableWidget_Enreg_doubleClicked(const QModelIndex &index)
{
    int ligne = index.row();
    ui->dateEdit_Livraison->setDate(modeleSelection->record(ligne).value(0).toDate());
    //ui->spinBox_stockInit->setValue(modeleSelection->record(ligne).value(1).toInt());
   // ui->spinBox_Entree->setValue(modeleSelection->record(ligne).value(2).toInt());
   // ui->spinBox_pnl->setValue(modeleSelection->record(ligne).value(3).toInt());
    //ui->spinBox_pcl->setValue(modeleSelection->record(ligne).value(4).toInt());
   // ui->lineEdit_stockRest->setText(modeleSelection->record(ligne).value(5).toString());
    ui->spinBox_pltNorLivre->setValue(modeleSelection->record(ligne).value(7).toInt());
    ui->spinBox_pltCasseLivre->setValue(modeleSelection->record(ligne).value(8).toInt());
    ui->lineEdit->setText(modeleSelection->record(ligne).value(11).toString());
    ui->textEdit_observation->setText(modeleSelection->record(ligne).value(12).toString());

    //ui->textEdit_Ramassage2->setText(modeleSelection->record(ligne).value(10).toString());
    numero=modeleSelection->record(ligne).value(13).toInt();
    ui->pushButtonModifier->setEnabled(true);
    ui->pushButtonEnregistrer->setDisabled(true);
}

void livraison::on_pushButtonModifier_clicked()
{

    reqinit.exec("SELECT pltMagasin, unitMagasin from livraison  order by idLivraison desc limit 1,1");
    reqentre.exec("SELECT nbPlOeuf, nbUnitOeuf from ramassage  WHERE dateRamassage= '"+QDate::currentDate().toString("yyyy-MM-dd")+"' ");
    modeleInit->setQuery(reqinit);
    modeleEntree->setQuery(reqentre);
    if(!reqinit.exec())
        QMessageBox::warning(this,"Erreur insertion livraison",reqinit.lastError().text());
    if(!reqentre.exec())
        QMessageBox::warning(this,"Erreur insertion livraison",reqentre.lastError().text());


    if(ui->lineEdit->text().isEmpty()){
            QMessageBox::critical(0,"Modification","Veuillez remplir tous les champs obligatoires SVP !");
    }else {
                requeteInsertion.prepare("UPDATE livraison SET dateLivraison='"+ui->dateEdit_Livraison->date().toString("yyyy-MM-dd")+"',"
                                         " pltInitiale=?, uniteInitiale=?, pltEntree=?, uniteEntree=?, pltDisponible=?, "
                                         " uniteDisponiible=?, nbPltNormLivre=?, nbPltCasseLivre=?, pltMagasin=?, unitMagasin=?, receveur=?,"
                                          "observation=?, idUser=? WHERE idLivraison='"+QString::number(numero)+"'");

                /*requeteInsertion.bindValue(0,ui->spinBox_stockInit->value());
                requeteInsertion.bindValue(1,ui->spinBox_Entree->value());
                requeteInsertion.bindValue(2,ui->spinBox_pnl->value());
                requeteInsertion.bindValue(3,ui->spinBox_pcl->value());
                int res=ui->spinBox_Entree->value()-(ui->spinBox_pcl->value()+ui->spinBox_pnl->value());
                requeteInsertion.bindValue(4,res);
                requeteInsertion.bindValue(5,ui->spinBox_pom->value());
               // requeteInsertion.bindValue(6,ui->spinBox_6->value());
                requeteInsertion.bindValue(6,ui->lineEdit->text());
                requeteInsertion.bindValue(7,ui->textEdit_observation->toPlainText());
                requeteInsertion.bindValue(8,idUser.toInt());
                requeteInsertion.bindValue(9,ui->spinBox_uom->value());*/

                requeteInsertion.bindValue(0,modeleInit->record(0).value(0).toInt());
                requeteInsertion.bindValue(1,modeleInit->record(0).value(1).toInt());
                requeteInsertion.bindValue(2,modeleEntree->record(0).value(0).toInt());
                requeteInsertion.bindValue(3,modeleEntree->record(0).value(1).toInt());//ddd
                //int val=200;
                //int result=ui->spinBox_Entree->value()-(ui->spinBox_pcl->value()+ui->spinBox_pnl->value());
                int resultat=0,unitRestant=0;
                if ((modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())>=30)
                {
                    resultat = (modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())/30;
                    unitRestant=(modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())-(resultat*30);
                }
                else
                    unitRestant=(modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt());
                requeteInsertion.bindValue(4,modeleInit->record(0).value(0).toInt()+ modeleEntree->record(0).value(0).toInt()+resultat);
                requeteInsertion.bindValue(5,unitRestant);
                requeteInsertion.bindValue(6,ui->spinBox_pltNorLivre->value());
                requeteInsertion.bindValue(7,ui->spinBox_pltCasseLivre->value());
                requeteInsertion.bindValue(8,(modeleInit->record(0).value(0).toInt()+ modeleEntree->record(0).value(0).toInt()+resultat)-(ui->spinBox_pltNorLivre->value()+ui->spinBox_pltCasseLivre->value()));
                requeteInsertion.bindValue(9,unitRestant);
                requeteInsertion.bindValue(10,ui->lineEdit->text());
                requeteInsertion.bindValue(11,ui->textEdit_observation->toPlainText());
                requeteInsertion.bindValue(12,idUser.toInt());

                if(!requeteInsertion.exec())
                    QMessageBox::warning(this,"Erreur modification reproduction",requeteInsertion.lastError().text());
                afficherInfo();
                effacerChamps();
          }
}

void livraison::on_comboBox_critere_currentTextChanged(const QString &arg1)
{
    if(arg1=="Période" or arg1=="Date enregistrement")
   {   ui->dateEdit_debut->setDate(QDate::currentDate());
       ui->dateEdit_fin->setDate(QDate::currentDate());
       ui->label_debut->show();
       ui->dateEdit_debut->show();
       ui->label_fin->show();
       ui->dateEdit_fin->show();
       ui->comboBox_liste_Critere->hide();
       ui->label_debut->setGeometry(300,10,110,31);
       ui->dateEdit_debut->setGeometry(400,10,110,31);
       ui->label_fin->setGeometry(520,10,110,31);
       ui->dateEdit_fin->setGeometry(600,10,110,31);
   }
   else
   {
       ui->comboBox_liste_Critere->clear();
       ui->comboBox_liste_Critere->addItem("");
       ui->comboBox_liste_Critere->show();
       ui->label_debut->hide();
       ui->label_fin->hide();
       ui->dateEdit_debut->hide();
       ui->dateEdit_fin->hide();
       ui->tableWidgetConsult->setRowCount(0);
        if(arg1=="Receveur")
       {
          requeteConsulter.exec("SELECT DISTINCT receveur FROM livraison");
       }
        else if(arg1=="Plateaux disponibles")
       {
          requeteConsulter.exec("SELECT DISTINCT pltDisponible FROM livraison");
       }
       else if(arg1=="Unités disponibles")
       {
          requeteConsulter.exec("SELECT DISTINCT uniteDisponiible FROM livraison");
       }
       else if(arg1=="Plateaux entrés")
       {
          requeteConsulter.exec("SELECT DISTINCT pltEntree FROM livraison");
       }
       else if(arg1=="Plateaux normals livrés")
       {
          requeteConsulter.exec("SELECT DISTINCT nbPltNormLivre FROM livraison");
       }
       else if(arg1=="Plateaux cassés livrés")
       {
          requeteConsulter.exec("SELECT DISTINCT nbPltCasseLivre FROM livraison");
       }


       modeleCritere->setQuery(requeteConsulter);
       int i = 0;
       while (requeteConsulter.next())
       {
           ui->comboBox_liste_Critere->addItem(modeleCritere->record(i).value(0).toString());
           i++;
       }
   }
}

void livraison::on_comboBox_liste_Critere_currentTextChanged(const QString &arg1)
{
    QString critere_Choix;
    critere_Choix=ui->comboBox_critere->currentText();
    if(critere_Choix=="Receveur")
    {
        requeteConsulter.prepare("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                                 "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                                 " observation, idLivraison FROM "
                                 "livraison WHERE receveur =? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if(critere_Choix=="Plateaux disponibles")
     {
        requeteConsulter.prepare("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                                 "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                                 " observation, idLivraison FROM "
                                 "livraison WHERE pltDisponible =? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if(critere_Choix=="Unités disponibles")
     {
        requeteConsulter.prepare("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                                 "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                                 " observation, idLivraison FROM "
                                 "livraison WHERE uniteDisponiible =? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if(critere_Choix=="Plateaux entrés")
     {
        requeteConsulter.prepare("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                                 "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                                 " observation, idLivraison FROM "
                                 "livraison WHERE pltEntree =? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if(critere_Choix=="Plateaux normals livrés")
    {
         requeteConsulter.prepare("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                                  "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                                  " observation, idLivraison FROM "
                                         "livraison WHERE nbPltNormLivre =? ");
          requeteConsulter.bindValue(0,arg1);
          if(!requeteConsulter.exec())
               QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
     }
    else if(critere_Choix=="Plateaux cassés livrés")
    {
         requeteConsulter.prepare("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                                  "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                                  " observation, idLivraison FROM "
                                  "livraison WHERE nbPltCasseLivre =? ");
         requeteConsulter.bindValue(0,arg1);
          if(!requeteConsulter.exec())
               QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
     }


    modelConsulter->setQuery(requeteConsulter);
     afficherInfo2();
     affich();
}
void livraison::afficherInfo2()
{
    ui->tableWidgetConsult->setColumnCount(13);
    QStringList entete;
    entete<<"DATE DE\nLIVRAISON"<<"PLATEAUX\nINITIAL"<<"UNITES\nINITIAL"<<"PLATEAUX\nENTRES"<<"UNITES\nENTRES"<<"PLATEAUX\nDISPONIBLES"<<"UNITES\nDISPONIBLES"
         <<"PLATEAUX\nNORMALS\nLIVRES"<<"PLATEAUX\nCASSES\nLIVRES"<<"PLATEAUX\nEN\nMAGASIN"<<"UNITES EN\nMAGASIN"<<"RECEVEUR"<<"OBSERVATION";
    if(requeteConsulter.size()==0){
        ui->tableWidgetConsult->clear();
        ui->tableWidgetConsult->setRowCount(0);
        //QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    ui->tableWidgetConsult->setHorizontalHeaderLabels(entete);
    //Entete des totaux
    QTableWidgetItem *itemeTotalPlateauNormalLivre = new QTableWidgetItem("");
    itemeTotalPlateauNormalLivre->setFlags(Qt::ItemIsEnabled);
    itemeTotalPlateauNormalLivre->setTextAlignment(Qt::AlignCenter);
    QTableWidgetItem *itemeTotalPlateauCassesLivre = new QTableWidgetItem("");
    itemeTotalPlateauCassesLivre->setFlags(Qt::ItemIsEnabled);
    itemeTotalPlateauCassesLivre->setTextAlignment(Qt::AlignCenter);

    //construction des items des totaux
    ui->tableWidget_total->setItem(0,0,itemeTotalPlateauNormalLivre);
    ui->tableWidget_total->setItem(0,1,itemeTotalPlateauCassesLivre);

     for (int i=0; i<requeteConsulter.size(); i++)
     {
         ui->tableWidgetConsult->setRowCount(i+1);

         QTableWidgetItem *itemDateLivraison = new QTableWidgetItem(modelConsulter->record(i).value(0).toString());
         itemDateLivraison->setFlags(Qt::ItemIsEnabled);
         itemDateLivraison->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itempltInitiale = new QTableWidgetItem(modelConsulter->record(i).value(1).toString());
         itempltInitiale->setFlags(Qt::ItemIsEnabled);
         itempltInitiale->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemuniteInitiale = new QTableWidgetItem(modelConsulter->record(i).value(2).toString());
         itemuniteInitiale->setFlags(Qt::ItemIsEnabled);
         itemuniteInitiale->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itempltEntree = new QTableWidgetItem(modelConsulter->record(i).value(3).toString());
         itempltEntree->setFlags(Qt::ItemIsEnabled);
         itempltEntree->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemuniteEntree = new QTableWidgetItem(modelConsulter->record(i).value(4).toString());
         itemuniteEntree->setFlags(Qt::ItemIsEnabled);
         itemuniteEntree->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itempltDisponible = new QTableWidgetItem(modelConsulter->record(i).value(5).toString());
         itempltDisponible->setFlags(Qt::ItemIsEnabled);
         itempltDisponible->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemuniteDisponiible = new QTableWidgetItem(modelConsulter->record(i).value(6).toString());
         itemuniteDisponiible->setFlags(Qt::ItemIsEnabled);
         itemuniteDisponiible->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemnbPltNormLivre = new QTableWidgetItem(modelConsulter->record(i).value(7).toString());
         itemnbPltNormLivre->setFlags(Qt::ItemIsEnabled);
         itemnbPltNormLivre->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemnbPltCasseLivre = new QTableWidgetItem(modelConsulter->record(i).value(8).toString());
         itemnbPltCasseLivre->setFlags(Qt::ItemIsEnabled);
         itemnbPltCasseLivre->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itempltMagasin = new QTableWidgetItem(modelConsulter->record(i).value(9).toString());
         itempltMagasin->setFlags(Qt::ItemIsEnabled);
         itempltMagasin->setTextAlignment(Qt::AlignCenter);

         QTableWidgetItem *itemunitMagasin = new QTableWidgetItem(modelConsulter->record(i).value(10).toString());
         itemunitMagasin->setFlags(Qt::ItemIsEnabled);
         itemunitMagasin->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemreceveur = new QTableWidgetItem(modelConsulter->record(i).value(11).toString());
         itemreceveur->setFlags(Qt::ItemIsEnabled);
         itemreceveur->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemobservation = new QTableWidgetItem(modelConsulter->record(i).value(12).toString());
         itemobservation->setFlags(Qt::ItemIsEnabled);
         itemobservation->setTextAlignment(Qt::AlignCenter);

         ui->tableWidgetConsult->setItem(i,0,itemDateLivraison);
         ui->tableWidgetConsult->setItem(i,1,itempltInitiale);
         ui->tableWidgetConsult->setItem(i,2,itemuniteInitiale);
         ui->tableWidgetConsult->setItem(i,3,itempltEntree);
         ui->tableWidgetConsult->setItem(i,4,itemuniteEntree);
         ui->tableWidgetConsult->setItem(i,5,itempltDisponible);
         ui->tableWidgetConsult->setItem(i,6,itemuniteDisponiible);
         ui->tableWidgetConsult->setItem(i,7,itemnbPltNormLivre);
         ui->tableWidgetConsult->setItem(i,8,itemnbPltCasseLivre);
         ui->tableWidgetConsult->setItem(i,9,itempltMagasin);
         ui->tableWidgetConsult->setItem(i,10,itemunitMagasin);
         ui->tableWidgetConsult->setItem(i,11,itemreceveur);
         ui->tableWidgetConsult->setItem(i,12,itemobservation);

         //affichage des totaux
         ui->tableWidget_total->item(0,0)->setText(QString::number(ui->tableWidget_total->item(0,0)->text().toInt()+itemnbPltNormLivre->text().toInt()));
         ui->tableWidget_total->item(0,1)->setText(QString::number(ui->tableWidget_total->item(0,1)->text().toInt()+itemnbPltCasseLivre->text().toInt()));

         /*QTableWidgetItem *itemDateLivraison = new QTableWidgetItem(modelConsulter->record(i).value(0).toString());
         itemDateLivraison->setFlags(Qt::ItemIsEnabled);
         itemDateLivraison->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemStockInit = new QTableWidgetItem(modelConsulter->record(i).value(1).toString());
         itemStockInit->setFlags(Qt::ItemIsEnabled);
         itemStockInit->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemStockEntree = new QTableWidgetItem(modelConsulter->record(i).value(2).toString());
         itemStockEntree->setFlags(Qt::ItemIsEnabled);
         itemStockEntree->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemPONL = new QTableWidgetItem(modelConsulter->record(i).value(3).toString());
         itemPONL->setFlags(Qt::ItemIsEnabled);
         itemPONL->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemPOCL = new QTableWidgetItem(modelConsulter->record(i).value(4).toString());
         itemPOCL->setFlags(Qt::ItemIsEnabled);
         itemPOCL->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemStockRestant = new QTableWidgetItem(modelConsulter->record(i).value(5).toString());
         itemStockRestant->setFlags(Qt::ItemIsEnabled);
         itemStockRestant->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemMagasin = new QTableWidgetItem(modelConsulter->record(i).value(6).toString());
         itemMagasin->setFlags(Qt::ItemIsEnabled);
         itemMagasin->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemReceveur = new QTableWidgetItem(modelConsulter->record(i).value(7).toString());
         itemReceveur->setFlags(Qt::ItemIsEnabled);
         itemReceveur->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemObservation = new QTableWidgetItem(modelConsulter->record(i).value(8).toString());
         itemObservation->setFlags(Qt::ItemIsEnabled);
         itemObservation->setTextAlignment(Qt::AlignCenter);

         ui->tableWidgetConsult->setItem(i,0,itemDateLivraison);
         ui->tableWidgetConsult->setItem(i,1,itemStockInit);
         ui->tableWidgetConsult->setItem(i,2,itemStockEntree);
         ui->tableWidgetConsult->setItem(i,3,itemPONL);
         ui->tableWidgetConsult->setItem(i,4,itemPOCL);
         ui->tableWidgetConsult->setItem(i,5,itemStockRestant);
         ui->tableWidgetConsult->setItem(i,6,itemMagasin);
         ui->tableWidgetConsult->setItem(i,7,itemReceveur);
         ui->tableWidgetConsult->setItem(i,8,itemObservation);
         ui->tableWidget_Enreg->setItem(i,9,itemUOM);
         ui->tableWidget_Enreg->setItem(i,10,itemObservation);*/
     }
     OutilsMimes::customizeTableWidget(ui->tableWidgetConsult);
     ui->pushButtonEnregistrer->setEnabled(true);
     ui->pushButtonModifier->setDisabled(true);
}

void livraison::on_dateEdit_debut_userDateChanged(const QDate &date)
{
    QString critereDate= ui->comboBox_critere->currentText();
        ui->dateEdit_fin->setDate(QDate::currentDate());
        if (date > ui->dateEdit_fin->date()){
        QMessageBox::information(0,"Erreur de date","La date de debut ne peut pas être supérieur à la date de fin");
        ui->tableWidgetConsult->setRowCount(0);
        }
        else
             {
            if(critereDate=="Période"){
        requeteConsulter.exec("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                              "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                              " observation, idLivraison FROM "
                              "livraison  WHERE date(dateLivraison) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                              "'"+ui->dateEdit_fin->date().toString("yyyy-MM-dd")+"' ");
                            }

            if(critereDate=="Date enregistrement"){
        requeteConsulter.exec("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                              "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                              " observation, idLivraison FROM "
                              "livraison WHERE date(dateEnregistrement) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                              "'"+ui->dateEdit_fin->date().toString("yyyy-MM-dd")+"' ");
                            }
            modelConsulter->setQuery(requeteConsulter);
            afficherInfo2();
             }
}

void livraison::on_dateEdit_fin_dateChanged(const QDate &date)
{
    QString critereDate= ui->comboBox_critere->currentText();
    if(date < ui->dateEdit_debut->date()){
          QMessageBox::information(0,"ERREUR DE DATE","La date de fin ne peut pas être inférieur à la date de début ");
          ui->tableWidgetConsult->setRowCount(0);
          }
          else{
        if(critereDate=="Période"){
    requeteConsulter.exec("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                          "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                          " observation, idLivraison FROM "
                          "livraison WHERE date(dateLivraison) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                          "'"+ui->dateEdit_debut->date().toString("yyyy-MM-dd")+"' ");
                        }

        if(critereDate=="Date enregistrement"){
    requeteConsulter.exec("SELECT date_format(dateLivraison,'%d/%m/%Y'), pltInitiale, uniteInitiale, pltEntree, uniteEntree, pltDisponible, "
                          "uniteDisponiible, nbPltNormLivre, nbPltCasseLivre, pltMagasin, unitMagasin, receveur,"
                          " observation, idLivraison FROM "
                          "livraison WHERE date(dateEnregistrement) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                          "'"+ui->dateEdit_debut->date().toString("yyyy-MM-dd")+"' ");
                        }
        modelConsulter->setQuery(requeteConsulter);
        afficherInfo2();
           }
}

void livraison::affich()
{

    QStringList entete2;
    ui->tableWidget_total->setColumnCount(2);
    entete2<<"TOTALS PLATEAUX NORMALS LIVRES"<<"TOTALS PLATEAUX CASSES LIVRES"<<"TAUX DE PONTE\nTOTAL";
    ui->tableWidget_total->setHorizontalHeaderLabels(entete2);


    ui->tableWidget_total->setRowCount(1);
    /*for (int i=0; i<requeteSomme.size(); i++)
    {

    QTableWidgetItem *itemsom = new QTableWidgetItem(modeleSomme->record(i).value(0).toString());
    itemsom->setFlags(Qt::ItemIsEnabled);
    itemsom->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget_total->setItem(i,0,itemsom);
     QMessageBox::critical(this,"erreur sélection information",requeteSomme.value(0).toString());

    }*/

    OutilsMimes::customizeTableWidget(ui->tableWidget_total);
}


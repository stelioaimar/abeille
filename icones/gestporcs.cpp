#include "gestporcs.h"
#include "ui_gestporcs.h"

Gestporcs::Gestporcs(QWidget *parent, QString currentUser) :
    QWidget(parent),
    ui(new Ui::Gestporcs)
{
    ui->setupUi(this);
    idUser = currentUser;
    ui->dateEdit_Porc->setDate(QDate::currentDate());
    //ui->dateEdit_debut->setDate(QDate::currentDate());
   // ui->dateEdit_fin->setDate(QDate::currentDate());
    modeleCritere= new QSqlQueryModel();
    modelConsulter = new QSqlQueryModel();
    ui->dateEdit_debut->hide();
    ui->dateEdit_fin->hide();
    ui->label_debut->hide();
    ui->label_fin->hide();
    afficherInfo2();



}

Gestporcs::~Gestporcs()
{
    delete ui;
}
void Gestporcs::changerOgletActif(int ind)
{
    ui->tabWidgetEnrCon->setCurrentIndex(ind);

}

void Gestporcs::afficherInfo2()
{
    QStringList entete;
    ui->tableWidget_Consult->setColumnCount(21);
    entete<<"DATE"<<"MALE"<<"FEMELLE"<<"SELECT\nENGRA"<<"MALE"<<"FEMELLE"<<"VENTES"<<"TOTAL"<<"EFF\nDEBUT"<<"PORCS\nNES"
         <<"SEVRES\nTRANSF"<<"MORTS"<<"EFF\nVIVANT"<<"ENGR\nESSE"<<"SEVRES\nRECU"<<"REFORM\nRECU"<<"SELECT\nREPRO"<<"VENTES"<<"MORTS"
        <<"TOTAL"<<"TOTAUX";
    if(requeteConsulter.size()==0){
        ui->tableWidget_Consult->clear();
        ui->tableWidget_Consult->setRowCount(0);
        //QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    ui->tableWidget_Consult->setHorizontalHeaderLabels(entete);
     for (int i=0; i<requeteConsulter.size(); i++)
     {
         ui->tableWidget_Consult->setRowCount(i+1);

         QTableWidgetItem *itemDatePorc = new QTableWidgetItem(modelConsulter->record(i).value(0).toString());
         itemDatePorc->setFlags(Qt::ItemIsEnabled);
         itemDatePorc->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemNbrPorcMal = new QTableWidgetItem(modelConsulter->record(i).value(1).toString());
         itemNbrPorcMal->setFlags(Qt::ItemIsEnabled);
         itemNbrPorcMal->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemNbrPorcFemelle = new QTableWidgetItem(modelConsulter->record(i).value(2).toString());
         itemNbrPorcFemelle->setFlags(Qt::ItemIsEnabled);
         itemNbrPorcFemelle->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemSelecEngrai = new QTableWidgetItem(modelConsulter->record(i).value(3).toString());
         itemSelecEngrai->setFlags(Qt::ItemIsEnabled);
         itemSelecEngrai->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemMalVieilleReforme = new QTableWidgetItem(modelConsulter->record(i).value(4).toString());
         itemMalVieilleReforme->setFlags(Qt::ItemIsEnabled);
         itemMalVieilleReforme->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemFemelleVieilleReforme = new QTableWidgetItem(modelConsulter->record(i).value(5).toString());
         itemFemelleVieilleReforme->setFlags(Qt::ItemIsEnabled);
         itemFemelleVieilleReforme->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemVentePorc = new QTableWidgetItem(modelConsulter->record(i).value(6).toString());
         itemVentePorc->setFlags(Qt::ItemIsEnabled);
         itemVentePorc->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemtotalRepro = new QTableWidgetItem(modelConsulter->record(i).value(7).toString());
         itemtotalRepro->setFlags(Qt::ItemIsEnabled);
         itemtotalRepro->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemEffecDebut = new QTableWidgetItem(modelConsulter->record(i).value(8).toString());
         itemEffecDebut->setFlags(Qt::ItemIsEnabled);
         itemEffecDebut->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemPorceletNes = new QTableWidgetItem(modelConsulter->record(i).value(9).toString());
         itemPorceletNes->setFlags(Qt::ItemIsEnabled);
         itemPorceletNes->setTextAlignment(Qt::AlignCenter);

         QTableWidgetItem *itemSevreTranferes = new QTableWidgetItem(modelConsulter->record(i).value(10).toString());
         itemSevreTranferes->setFlags(Qt::ItemIsEnabled);
         itemSevreTranferes->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemPorceletMorts = new QTableWidgetItem(modelConsulter->record(i).value(11).toString());
         itemPorceletMorts->setFlags(Qt::ItemIsEnabled);
         itemPorceletMorts->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemTotalPorcelets = new QTableWidgetItem(modelConsulter->record(i).value(12).toString());
         itemTotalPorcelets->setFlags(Qt::ItemIsEnabled);
         itemTotalPorcelets->setTextAlignment(Qt::AlignCenter);

         QTableWidgetItem *itemEngresse = new QTableWidgetItem(modelConsulter->record(i).value(13).toString());
         itemEngresse->setFlags(Qt::ItemIsEnabled);
         itemEngresse->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemSevreRecu = new QTableWidgetItem(modelConsulter->record(i).value(14).toString());
         itemSevreRecu->setFlags(Qt::ItemIsEnabled);
         itemSevreRecu->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemReformRecuRepro = new QTableWidgetItem(modelConsulter->record(i).value(15).toString());
         itemReformRecuRepro->setFlags(Qt::ItemIsEnabled);
         itemReformRecuRepro->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemSelectRepro = new QTableWidgetItem(modelConsulter->record(i).value(16).toString());
         itemSelectRepro->setFlags(Qt::ItemIsEnabled);
         itemSelectRepro->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemVentes = new QTableWidgetItem(modelConsulter->record(i).value(17).toString());
         itemVentes->setFlags(Qt::ItemIsEnabled);
         itemVentes->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemMorts = new QTableWidgetItem(modelConsulter->record(i).value(18).toString());
         itemMorts->setFlags(Qt::ItemIsEnabled);
         itemMorts->setTextAlignment(Qt::AlignCenter);
         QTableWidgetItem *itemTotalEngr = new QTableWidgetItem(modelConsulter->record(i).value(19).toString());
         itemTotalEngr->setFlags(Qt::ItemIsEnabled);
         itemTotalEngr->setTextAlignment(Qt::AlignCenter);

         QTableWidgetItem *itemTotalGeneral = new QTableWidgetItem(modelConsulter->record(i).value(20).toString());
         itemTotalGeneral->setFlags(Qt::ItemIsEnabled);
         itemTotalGeneral->setTextAlignment(Qt::AlignCenter);

         ui->tableWidget_Consult->setItem(i,0,itemDatePorc);
         ui->tableWidget_Consult->setItem(i,1,itemNbrPorcMal);
         ui->tableWidget_Consult->setItem(i,2,itemNbrPorcFemelle);
         ui->tableWidget_Consult->setItem(i,3,itemSelecEngrai);
         ui->tableWidget_Consult->setItem(i,4,itemMalVieilleReforme);
         ui->tableWidget_Consult->setItem(i,5,itemFemelleVieilleReforme);
         ui->tableWidget_Consult->setItem(i,6,itemVentePorc);
         ui->tableWidget_Consult->setItem(i,7,itemtotalRepro);
         ui->tableWidget_Consult->setItem(i,8,itemEffecDebut);
         ui->tableWidget_Consult->setItem(i,9,itemPorceletNes);
         ui->tableWidget_Consult->setItem(i,10,itemSevreTranferes);
         ui->tableWidget_Consult->setItem(i,11,itemPorceletMorts);
         ui->tableWidget_Consult->setItem(i,12,itemTotalPorcelets);

         ui->tableWidget_Consult->setItem(i,13,itemEngresse);
         ui->tableWidget_Consult->setItem(i,14,itemSevreRecu);
         ui->tableWidget_Consult->setItem(i,15,itemReformRecuRepro);
         ui->tableWidget_Consult->setItem(i,16,itemSelectRepro);
         ui->tableWidget_Consult->setItem(i,17,itemVentes);
         ui->tableWidget_Consult->setItem(i,18,itemMorts);
         ui->tableWidget_Consult->setItem(i,19,itemTotalEngr);
         ui->tableWidget_Consult->setItem(i,20,itemTotalGeneral);

         //ui->tableWidget_Consult->setItem(i,19,itemTotalEngr);
         //ui->tableWidget_Consult->setItem(i,20,itemTotalGeneral);


     }

     OutilsMimes::customizeTableWidget(ui->tableWidget_Consult);
     ui->pushButtonEnregistrer->setEnabled(true);
     ui->pushButtonModifier->setDisabled(true);
}

void Gestporcs::on_pushButtonEnregistrer_clicked()
{
    reqInsertion.prepare("INSERT into gestporcs(datePorc, nbrPorcMale, nbrPorcFemelle, selecEngrai, malVieilReforme, femelleVieilReforme, ventePorc, totalRepro,"
                         " effDebut, porceletNes, sevreTransferes, porceletMorts, totalPorcelets,"
                         " engresse, sevreRecu, reformRecuRepro, selectRepro, ventes, Morts, totaltEngre, totalGeneral, dateEnregistrement, idUser)"
                         "VALUES('"+ui->dateEdit_Porc->date().toString("yyyy-MM-dd")+"',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'"+QDate::currentDate().toString("yyyy-MM-dd")+"',?)");
    int total1=(ui->spinBox_PorcherieMal->value()+ui->spinBox_PorcherieFemelle->value()+ui->spinBox_seletEngraissement->value())-(ui->spinBox_MalVieilReformes->value()+ui->spinBox_FemelleVieilReformes->value()+ui->spinBox_VenteRepro->value());
    int total2=(ui->spinBox_EffDebut->value()+ui->spinBox_PorceletsNes->value())-(ui->spinBox_SevresTranferes->value()+ui->spinBox_PoceletsMorts->value());
    int total3=(ui->spinBox_Engresse->value()+ui->spinBox_SevreRecu->value()+ui->spinBox_ReformesRecuRepro->value())-(ui->spinBox_SelectRepro->value()+ui->spinBox_Ventes->value()+ui->spinBox_Morts->value());
    reqInsertion.bindValue(0,ui->spinBox_PorcherieMal->value());
    reqInsertion.bindValue(1,ui->spinBox_PorcherieFemelle->value());
    reqInsertion.bindValue(2,ui->spinBox_seletEngraissement->value());
    reqInsertion.bindValue(3,ui->spinBox_MalVieilReformes->value());
    reqInsertion.bindValue(4,ui->spinBox_FemelleVieilReformes->value());
    reqInsertion.bindValue(5,ui->spinBox_VenteRepro->value());
    reqInsertion.bindValue(6,total1);
    reqInsertion.bindValue(7,ui->spinBox_EffDebut->value());
    reqInsertion.bindValue(8,ui->spinBox_PorceletsNes->value());
    reqInsertion.bindValue(9,ui->spinBox_SevresTranferes->value());
    reqInsertion.bindValue(10,ui->spinBox_PoceletsMorts->value());
    reqInsertion.bindValue(11,total2);
    reqInsertion.bindValue(12,ui->spinBox_Engresse->value());
    reqInsertion.bindValue(13,ui->spinBox_SevreRecu->value());
    reqInsertion.bindValue(14,ui->spinBox_ReformesRecuRepro->value());
    reqInsertion.bindValue(15,ui->spinBox_SelectRepro->value());
    reqInsertion.bindValue(16,ui->spinBox_Ventes->value());
    reqInsertion.bindValue(17,ui->spinBox_Morts->value());
    reqInsertion.bindValue(18,total3);
    reqInsertion.bindValue(19,total1+total2+total3);
    reqInsertion.bindValue(20,idUser.toInt());

    if(!reqInsertion.exec())
        QMessageBox::information(0,"Erreur insertion porcs",reqInsertion.lastError().text());
}

void Gestporcs::on_comboBox_critere_currentTextChanged(const QString &arg1)
{
    if(arg1=="Date" or arg1=="Date enregistrement")
   {   ui->dateEdit_debut->setDate(QDate::currentDate());
       ui->dateEdit_fin->setDate(QDate::currentDate());
       ui->label_debut->show();
       ui->dateEdit_debut->show();
       ui->label_fin->show();
       ui->dateEdit_fin->show();
       ui->comboBox_liste_Critere->hide();
       ui->label_debut->setGeometry(300,10,110,31);
       ui->dateEdit_debut->setGeometry(400,10,110,31);
       ui->label_fin->setGeometry(520,10,110,31);
       ui->dateEdit_fin->setGeometry(600,10,110,31);

   }
   else
   {
       ui->comboBox_liste_Critere->clear();
       ui->comboBox_liste_Critere->addItem("");
       ui->comboBox_liste_Critere->show();
       ui->label_debut->hide();
       ui->label_fin->hide();
       ui->dateEdit_debut->hide();
       ui->dateEdit_fin->hide();
        if(arg1=="Total reproduction")
       {
          requeteConsulter.exec("SELECT DISTINCT totalRepro FROM gestporcs");
       }
        else if(arg1=="Bande")
        {
            requeteConsulter.exec("SELECT DISTINCT bande FROM ramassage");
        }
        else if(arg1=="Ventes")
        {
            requeteConsulter.exec("SELECT DISTINCT ventes FROM ramassage");
        }
        else if(arg1=="Morts")
        {
            requeteConsulter.exec("SELECT DISTINCT morts FROM ramassage");
        }
        else if(arg1=="Effectif début")
        {
            requeteConsulter.exec("SELECT DISTINCT effDebut FROM ramassage");
        }
        else if(arg1=="Effectif fin")
        {
            requeteConsulter.exec("SELECT DISTINCT effFin FROM ramassage");
        }
        else if(arg1=="Plateaux d'oeufs")
        {
            requeteConsulter.exec("SELECT DISTINCT nbPlOeuf FROM ramassage");
        }
        else if(arg1=="Nombre d'oeufs")
        {
            requeteConsulter.exec("SELECT DISTINCT nbOeuf FROM ramassage");
        }
        else if(arg1=="Aliment servi en KG")
        {
            requeteConsulter.exec("SELECT DISTINCT alimentKg FROM ramassage");
        }
        else if(arg1=="Unités d'oeufs")
        {
            requeteConsulter.exec("SELECT DISTINCT nbUnitOeuf FROM ramassage");
        }
        else if(arg1=="Taux de ponte")
        {
            requeteConsulter.exec("SELECT DISTINCT tauxPonte FROM ramassage");
        }





       modeleCritere->setQuery(requeteConsulter);
       int i = 0;
       while (requeteConsulter.next())
       {
           ui->comboBox_liste_Critere->addItem(modeleCritere->record(i).value(0).toString());
           i++;
       }
   }
}

void Gestporcs::on_comboBox_liste_Critere_currentTextChanged(const QString &arg1)
{
    QString critere_Choix;
    critere_Choix=ui->comboBox_critere->currentText();
    if(critere_Choix=="Batiment")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE batiment=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if(critere_Choix=="Bande")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE bande=? ");
        //requeteSomme.prepare("SELECT sum(effDebut) FROM ramassage WHERE bande=? ");
        requeteConsulter.bindValue(0,arg1);
        //requeteSomme.bindValue(0,arg1);
        //QMessageBox::critical(this,"erreur sélection information",modeleSomme->record(0).value(0).toString());
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());

    }
    else if (critere_Choix=="Total reproduction")
    {
        requeteConsulter.prepare("SELECT date_format(datePorc,'%d/%m/%Y'),nbrPorcMale, nbrPorcFemelle, selecEngrai, malVieilReforme, femelleVieilReforme, ventePorc, totalRepro,"
                                 " effDebut, porceletNes, sevreTransferes, porceletMorts, totalPorcelets,"
                                 " engresse, sevreRecu, reformRecuRepro, selectRepro, ventes, Morts, totaltEngre, totalGeneral, idPorcs FROM "
                                 "gestporcs WHERE totalRepro=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Morts")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE morts=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Effectif début")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE effDebut=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Effectif fin")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE effFin=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Plateaux d'oeufs")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE nbPlOeuf=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Unités d'oeufs")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE nbUnitOeuf=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Nombre d'oeufs")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE nbOeuf=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Aliment servi en KG")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE alimentKg=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if (critere_Choix=="Taux de ponte")
    {
        requeteConsulter.prepare("SELECT date_format(dateRamassage,'%d/%m/%Y'),bande, batiment, effDebut, ventes, morts, effFin, alimentKg, "
                                 "nbPlOeuf, nbUnitOeuf, nbOeuf, tauxPonte, observationRamasage, idRamassage FROM "
                                 "ramassage WHERE tauxPonte=? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }



    modelConsulter->setQuery(requeteConsulter);
     afficherInfo2();
}

void Gestporcs::on_dateEdit_fin_dateChanged(const QDate &date)
{
    QString critereDate= ui->comboBox_critere->currentText();
    if(date < ui->dateEdit_debut->date()){
          QMessageBox::information(0,"ERREUR DE DATE","La date de fin ne peut pas être inférieur à la date de début ");
           ui->tableWidget_Consult->setRowCount(0);
          }
    else{
        if(critereDate=="Date"){
    requeteConsulter.exec("SELECT date_format(datePorc,'%d/%m/%Y'),nbrPorcMale, nbrPorcFemelle, selecEngrai, malVieilReforme, femelleVieilReforme, ventePorc, totalRepro,"
                          " effDebut, porceletNes, sevreTransferes, porceletMorts, totalPorcelets,"
                          " engresse, sevreRecu, reformRecuRepro, selectRepro, ventes, Morts, totaltEngre, totalGeneral, idPorcs FROM "
                          "gestporcs WHERE date(datePorc) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                          "'"+ui->dateEdit_debut->date().toString("yyyy-MM-dd")+"' ");

    if(!requeteConsulter.exec())
        QMessageBox::information(0,"erreur Findate",requeteConsulter.lastError().text());
                        }

        if(critereDate=="Date enregistrement"){
    requeteConsulter.exec("SELECT date_format(datePorc,'%d/%m/%Y'),nbrPorcMale, nbrPorcFemelle, selecEngrai, malVieilReforme, femelleVieilReforme, ventePorc, totalRepro,"
                          " effDebut, porceletNes, sevreTransferes, porceletMorts, totalPorcelets,"
                          " engresse, sevreRecu, reformRecuRepro, selectRepro, ventes, Morts, totaltEngre, totalGeneral, idPorcs FROM "
                          "gestporcs  WHERE date(dateEnregistrement) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                          "'"+ui->dateEdit_debut->date().toString("yyyy-MM-dd")+"' ");

    if(!requeteConsulter.exec())
        QMessageBox::information(0,"erreur Findate",requeteConsulter.lastError().text());
                        }
        modelConsulter->setQuery(requeteConsulter);
        afficherInfo2();

           }
}

void Gestporcs::on_dateEdit_debut_dateChanged(const QDate &date)
{
    QString critereDate= ui->comboBox_critere->currentText();
        ui->dateEdit_fin->setDate(QDate::currentDate());
        if (date > ui->dateEdit_fin->date()){
        QMessageBox::information(0,"Erreur de date","La date de debut ne peut pas être supérieur à la date de fin");
         ui->tableWidget_Consult->setRowCount(0);
        }
        else
             {
            if(critereDate=="Date"){
        requeteConsulter.exec("SELECT date_format(datePorc,'%d/%m/%Y'),nbrPorcMale, nbrPorcFemelle, selecEngrai, malVieilReforme, femelleVieilReforme, ventePorc, totalRepro,"
                              " effDebut, porceletNes, sevreTransferes, porceletMorts, totalPorcelets,"
                              " engresse, sevreRecu, reformRecuRepro, selectRepro, ventes, Morts, totaltEngre, totalGeneral, idPorcs FROM "
                              "gestporcs  WHERE date(datePorc) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                              "'"+ui->dateEdit_fin->date().toString("yyyy-MM-dd")+"' ");
        if(!requeteConsulter.exec())
            QMessageBox::information(0,"erreur Findate",requeteConsulter.lastError().text());
                            }

            if(critereDate=="Date enregistrement"){
        requeteConsulter.exec("SELECT date_format(datePorc,'%d/%m/%Y'),nbrPorcMale, nbrPorcFemelle, selecEngrai, malVieilReforme, femelleVieilReforme, ventePorc, totalRepro,"
                              " effDebut, porceletNes, sevreTransferes, porceletMorts, totalPorcelets,"
                              " engresse, sevreRecu, reformRecuRepro, selectRepro, ventes, Morts, totaltEngre, totalGeneral, idPorcs FROM "
                              "gestporcs WHERE date(dateEnregistrement) BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                              "'"+ui->dateEdit_fin->date().toString("yyyy-MM-dd")+"' ");

        if(!requeteConsulter.exec())
            QMessageBox::information(0,"erreur FindateEn",requeteConsulter.lastError().text());
                            }

            modelConsulter->setQuery(requeteConsulter);
            afficherInfo2();
             }
}

#include "accueil.h"
#include "ui_accueil.h"

Accueil::Accueil(QWidget *parent, QString currentUser ) :
    QMainWindow(parent),
    ui(new Ui::Accueil)
{
    ui->setupUi(this);
    QSqlQuery requetUser;
    requetUser.exec("Select nomUser, prenomUser from Utilisateur where idUser='"+currentUser+"'");
    if(requetUser.next())
        this->setWindowTitle("Utilisateur connecté : "+requetUser.value(0).toString()+" "+requetUser.value(1).toString());

   /*
    PageRamassage = new Ramassage(this, currentUser);
    PageVolaile = new Volaille(this, currentUser);
    PageGestLapin = new GestLapin(this, currentUser);
    PageRamassage->hide();
    PageVolaile->hide();
    PageGestLapin->hide();
    PageGestPorcs = new Gestporcs(this, currentUser);
    PageGestPorcs->hide();*/
    PageGestReproduction = new GestReproduction(this, currentUser);
    PageGestReproduction->hide();
    PageLivraison = new livraison(this, currentUser);
    PageLivraison->hide();

    idUser=currentUser;

    mainToolBar=new QToolBar(this);
    mainToolBar->setGeometry(2,21,1290,10);
    mainToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    mainToolBar->setFont(QFont("Times New Roman", 8, 6));
    mainToolBar->setIconSize(QSize(50,30));

    menu_GestReproduction = new QMenu();
    menu_GestPorcs = new QMenu();
     /*menu_Ramassage = new QMenu();
    menu_Volaille = new QMenu();*/
    //menu_Facture = new QMenu();
    //menuParametres = new QMenu();
    //menu_Livraison = new QMenu();

   //Accueil
      actionAccueil = new QAction(QIcon(":/icones/accueil1.png"), "ACCUEIL", this);
      mainToolBar->addAction(actionAccueil);
      connect(actionAccueil, SIGNAL(triggered(bool)), this, SLOT(afficherAccueil()));
      mainToolBar->addSeparator();

      //VOLAILLE
      /*QToolButton* toolButton_5 = new QToolButton();
      action_enregistrement_volaille = new QAction("ENREGISTREMENT", this);
      action_listes_volaille = new QAction("LISTES", this);
      menu_Volaille->addAction(action_enregistrement_volaille);
      menu_Volaille->addAction(action_listes_volaille);
      toolButton_5->setMenu(menu_Volaille);
      OutilsMimes::customizeToolButton(toolButton_5, "VOLAILLE","://icones/image ferme5.jpg");
      toolButtonAction_5 = new QWidgetAction(this);
      toolButtonAction_5->setDefaultWidget(toolButton_5);
      mainToolBar->addAction(toolButtonAction_5);
      mainToolBar->addSeparator();*/

      //RAMASSAGE
     /* QToolButton* toolButton_2 = new QToolButton();
      action_enregistrement_ramassage = new QAction("ENREGISTREMENT", this);
      action_listes_ramassage = new QAction("LISTES", this);
      menu_Ramassage->addAction(action_enregistrement_ramassage);
      menu_Ramassage->addAction(action_listes_ramassage);
      toolButton_2->setMenu(menu_Ramassage);
      OutilsMimes::customizeToolButton(toolButton_2, "RAMASSAGE","://icones/image ferme5.jpg");
      toolButtonAction_2 = new QWidgetAction(this);
      toolButtonAction_2->setDefaultWidget(toolButton_2);
      mainToolBar->addAction(toolButtonAction_2);
      mainToolBar->addSeparator();*/

      //LIVRAISON
      /*QToolButton* toolButton_3 = new QToolButton();
      action_enregistrement_livraison = new QAction("ENREGISTREMENT", this);
      action_listes_livraison = new QAction("LISTES", this);
      menu_Livraison->addAction(action_enregistrement_livraison);
      menu_Livraison->addAction(action_listes_livraison);
      toolButton_3->setMenu(menu_Livraison);
      OutilsMimes::customizeToolButton(toolButton_3, "PORCS","://icones/image ferme9.jpg");
      toolButtonAction_3 = new QWidgetAction(this);
      toolButtonAction_3->setDefaultWidget(toolButton_3);
      mainToolBar->addAction(toolButtonAction_3);*/
     // mainToolBar->addSeparator();

      //GEST REPRODUCTION
         QToolButton* toolButton_1 = new QToolButton();
         action_enregistrement_gestrepro = new QAction("ENREGISTREMENT", this);
         action_listes_gestrepro = new QAction("LISTES", this);
         menu_GestReproduction->addAction(action_enregistrement_gestrepro);
         menu_GestReproduction->addAction(action_listes_gestrepro);
         toolButton_1->setMenu(menu_GestReproduction);
         OutilsMimes::customizeToolButton(toolButton_1, "RUCHES","://icones/absicone1.png");
         toolButtonAction_1 = new QWidgetAction(this);
         toolButtonAction_1->setDefaultWidget(toolButton_1);
         mainToolBar->addAction(toolButtonAction_1);

      //GestPorcs
      QToolButton* toolButton_4 = new QToolButton();
      action_enregistrementPorcs = new QAction("ENREGISTREMENT", this);
      action_ConsulterPorcs = new QAction("LISTES", this);
      menu_GestPorcs->addAction(action_enregistrementPorcs);
      menu_GestPorcs->addAction(action_ConsulterPorcs);
      toolButton_4->setMenu(menu_GestPorcs);
      OutilsMimes::customizeToolButton(toolButton_4, "VISITES","://icones/absicone2.png");
      toolButtonAction_4 = new QWidgetAction(this);
      toolButtonAction_4->setDefaultWidget(toolButton_4);
      mainToolBar->addAction(toolButtonAction_4);
      mainToolBar->addSeparator();


      /*FACTURE
      QToolButton* toolButton_3 = new QToolButton();
      action_enregistrement_facture = new QAction("ENREGISTREMENT", this);
      action_listes_facture = new QAction("LISTES", this);
      menu_Facture->addAction(action_enregistrement_facture);
      menu_Facture->addAction(action_listes_facture);
      toolButton_3->setMenu(menu_Facture);
      OutilsMimes::customizeToolButton(toolButton_3, "FACTURE",":/toolbar/icones/facture.png");
      toolButtonAction_3 = new QWidgetAction(this);
      toolButtonAction_3->setDefaultWidget(toolButton_3);
      mainToolBar->addAction(toolButtonAction_3);*/

      /*connect(action_enregistrement_gestrepro, SIGNAL(triggered(bool)), this, SLOT(on_actionOPERATION_triggered()));
      connect(action_enregistrement_ramassage, SIGNAL(triggered(bool)), this, SLOT(on_actionOPERat_triggered()));
      connect(action_enregistrement_volaille, SIGNAL(triggered(bool)), this, SLOT(action_enregistrementVolaille_triggered()));
      connect(action_listes_gestrepro, SIGNAL(triggered(bool)), this, SLOT(on_actionCONSULTATION_triggered()));
      connect(action_listes_ramassage, SIGNAL(triggered(bool)), this, SLOT(on_actionCONSUL_triggered()));
      connect(action_listes_volaille, SIGNAL(triggered(bool)), this, SLOT(action_ListeVolaille_triggered()));*/
      connect(action_enregistrement_gestrepro, SIGNAL(triggered(bool)), this, SLOT(on_action_enregistrement_gestrepro_triggered()));
      connect(action_listes_gestrepro, SIGNAL(triggered(bool)), this, SLOT(on_action_listes_gestrepro_triggered()));
      connect(action_enregistrementPorcs, SIGNAL(triggered(bool)), this, SLOT(action_enregistrementPorcs_triggered()));
      connect(action_ConsulterPorcs, SIGNAL(triggered(bool)), this, SLOT(action_ConsulterPorcs_triggered()));
      mainToolBar->addSeparator();

      //Paramètres
      /*action_ParametreArchive = new QAction("ARCHIVAGE", this);
      action_Operation = new QAction("OPERATION PRELIMINAIRE", this);
      action_Utilisateurs = new QAction("UTILISATEUR", this);
      //action_Profils = new QAction("Profils", this);
      menuParametres = new QMenu("Aide");
      menuParametres->addAction(action_ParametreArchive);
      menuParametres->addAction(action_Operation);
      menuParametres->addAction(action_Utilisateurs);
      //menuParametres->addAction(action_Profils);
      QToolButton *toolButton_4 = new QToolButton();
      toolButton_4->setMenu(menuParametres);
      OutilsMimes::customizeToolButton(toolButton_4, "PARAMETRE",":/toolbar/icones/param_icon.png");
      toolButtonAction_4 = new QWidgetAction(this);
      toolButtonAction_4->setDefaultWidget(toolButton_4);
      mainToolBar->addAction(toolButtonAction_4);
      connect(action_ParametreArchive, SIGNAL(triggered(bool)), this, SLOT(on_actionParametre_archive_triggered()));
      connect(action_Operation,SIGNAL(triggered(bool)), this, SLOT(on_actionParametre_operation_triggered()));
      connect(action_Utilisateurs,SIGNAL(triggered(bool)), this, SLOT(on_actionAJOUTER_UTILISATEUR_triggered()));*/

      //Deconnexion
       actionDeconnexion = new QAction(QIcon(":/toolbar/icones/deconnexion.png"), "Déconnexion", this);
       actionDeconnexion->setIconText("DECONNEXION");
       actionDeconnexion->setFont(QFont("Calibri", 12,6));
       mainToolBar->addAction(actionDeconnexion);
       mainToolBar->addSeparator();
       connect(actionDeconnexion, SIGNAL(triggered(bool)), this, SLOT(on_actionDECONNECTE_triggered()));
      //Quitter
      actionQuitter = new QAction(QIcon(":/toolbar/icones/ico_quitter.png"), "Quitter", this);
      actionQuitter->setIconText("Quitter");
      actionQuitter->setFont(QFont("Calibri", 12, 6));
      mainToolBar->addAction(actionQuitter);
      connect(actionQuitter, SIGNAL(triggered(bool)), this, SLOT(close()));
      mainToolBar->show();
      this->addToolBar(mainToolBar);
}

Accueil::~Accueil()
{
    delete ui;
}

void Accueil::on_actionOPERATION_triggered()
{
    /*PageLivraison->hide();
    PageRamassage->hide();
    PageGestReproduction->hide();
    PageGestPorcs->hide();
    PageVolaile->hide();
    PageGestLapin->hide();
    ui->label->hide();
    PageGestReproduction = new GestReproduction(this, idUser);
    PageGestReproduction->setGeometry(9,30,1330,650);
    PageGestReproduction->changerOgletActif(0);
    PageGestReproduction->show();*/
}

void Accueil::on_actionCONSULTATION_triggered()
{

    /*PageLivraison->hide();
    PageRamassage->hide();
    PageGestReproduction->hide();
    PageGestPorcs->hide();
    PageVolaile->hide();
    PageGestLapin->hide();
    ui->label->hide();
    PageGestLapin = new GestLapin(this, idUser);
    PageGestLapin->setGeometry(9,30,1330,650);
    PageGestLapin->changerOgletActif(0);
    PageGestLapin->show();*/
}

void Accueil::on_actionOPERat_triggered()
{

    /*PageRamassage->hide();
    PageGestReproduction->hide();
    PageLivraison->hide();
    PageGestPorcs->hide();
    PageVolaile->hide();
    PageGestLapin->hide();
    ui->label->hide();
    PageRamassage = new Ramassage(this, idUser);
    PageRamassage->setGeometry(9,30,1330,650);
    PageRamassage->changerOgletActif(0);
    PageRamassage->show();*/

}
void Accueil::on_actionCONSUL_triggered()
{
    /*pageConsultation = new Consultation(this, idUser);
    pagePatient->hide();
    pageFacture->hide();
    ui->label->hide();
    ui->label_2->hide();
    ui->label_3->hide();
   // pageParametre->hide();
    pageConsultation->setGeometry(9,30,1330,650);
    pageConsultation->changerOgletActif(1);
    pageConsultation->show();*/

    /*PageLivraison->hide();
    PageRamassage->hide();
    PageGestReproduction->hide();
    PageGestPorcs->hide();
    PageVolaile->hide();
    PageGestLapin->hide();
    ui->label->hide();
    PageRamassage = new Ramassage(this, idUser);
    PageRamassage->setGeometry(9,30,1330,650);
    PageRamassage->changerOgletActif(1);
    PageRamassage->show();*/
}

void Accueil::on_actionParametre_operation_triggered()
{
   /*pageUtilisateur = new User(this);
   pageCourierEntrant->hide();
   pageCourierSortant->hide();
   pageArchives->hide();
   ui->label->hide();
   pageParametre->hide();
   pageUtilisateur->setGeometry(9,30,1330,650);
   pageUtilisateur->changerOngletActif(0);
   pageUtilisateur->show();*/
}

void Accueil::on_actionAJOUTER_UTILISATEUR_triggered()
{
   /*pageParametre= new Parametres(this, idUser);
   pageCourierEntrant->hide();
   pageCourierSortant->hide();
   pageArchives->hide();
   pageArchivage->hide();
   ui->label->hide();
   pageUtilisateur->hide();
   pageParametre->setGeometry(250,70,1330,650);
   pageParametre->changerOngletActif(0);
   pageParametre->show();*/
}

void Accueil::on_actionRECHERCHE_triggered()
{
    /*pagePatient->hide();
    pageConsultation->hide();
    pageFacture->hide();
    ui->label->hide();
    //pageParametre->hide();
    pageUtilisateur->setGeometry(9,30,1330,650);
    pageUtilisateur->changerOngletActif(1);
    pageUtilisateur->show();*/
}

void Accueil::on_actionQUITTER_triggered()
{
    this->close();
}

void Accueil::on_actionDECONNECTE_triggered()
{
    authentification *PageAuthentification = new authentification;
    this->hide();
    PageAuthentification->show();
}

void Accueil::on_actionParametre_archive_triggered()
{
    /*pageFacture= new Facture(this);
    pagePatient->hide();
    pageConsultation->hide();
    pageParametre->hide();
    pageArchives->hide();
    ui->label->show();
    pageArchivage->show();
    //pageArchives->changerOgletActif(0);*/

}

void Accueil::afficherAccueil()
{
    /*pageArchivage->hide();
    pageParametre->hide();*/
    ui->label->show();
    /*ui->label_2->show();
    ui->label_3->show();*/
    this->show();
    PageGestReproduction->hide();
    /*PageRamassage->hide();
    PageGestPorcs->hide();
    PageVolaile->hide();*/
    //PageGestLapin->hide();
   /* pageConsultation->hide();
    pageFacture->hide();*/
    //PageLivraison->hide();
}

void Accueil::on_actionConsultation_de_l_archive_triggered()
{
    /*pageCourierEntrant->hide();
    pageCourierSortant->hide();
    pageUtilisateur->hide();
    pageArchives->show();
    pageArchivage->show();*/
}

void Accueil::on_actionEnreFacture_triggered()
{
        /*pageFacture = new Facture(this, idUser);
        pagePatient->hide();
        pageConsultation->hide();
        //pageParametre->hide();
        ui->label->hide();
        ui->label_2->hide();
        ui->label_3->hide();
        pageFacture->setGeometry(9,30,1330,650);
        pageFacture->changerOgletActif(0);
        pageFacture->show();*/
}

void Accueil::on_actionConsulter_archives_triggered()
{
        /*pageFacture = new Facture(this, idUser);
        pagePatient->hide();
        pageConsultation->hide();
        //pageParametre->hide();
        ui->label->hide();
        ui->label_2->hide();
        ui->label_3->hide();
        pageFacture->setGeometry(9,30,1330,650);
        pageFacture->changerOgletActif(1);
        pageFacture->show();*/
}

void Accueil::on_action_enregistrement_gestrepro_triggered()
{
    PageGestReproduction->hide();
    /*PageRamassage->hide();
    PageGestPorcs->hide();
    PageLivraison->hide();
    PageVolaile->hide();
    PageGestLapin->hide();*/
    ui->label->hide();
    PageGestReproduction = new GestReproduction(this, idUser);
    PageGestReproduction->setGeometry(9,30,1330,650);
    PageGestReproduction->changerOgletActif(0);
    PageGestReproduction->show();

}

void Accueil::on_action_listes_gestrepro_triggered()
{
    PageGestReproduction->hide();
    /*PageRamassage->hide();
    PageGestPorcs->hide();
    PageLivraison->hide();
    PageVolaile->hide();
    PageGestLapin->hide();*/
    ui->label->hide();
    PageGestReproduction = new GestReproduction(this, idUser);
    ui->label->hide();
    PageGestReproduction->setGeometry(9,30,1330,650);
    PageGestReproduction->changerOgletActif(1);
    PageGestReproduction->show();
}

void Accueil::action_enregistrementPorcs_triggered()
{
   PageLivraison->hide();
     /*PageRamassage->hide();*/
    PageGestReproduction->hide();
    //PageGestPorcs->hide();
    /*PageVolaile->hide();
    PageGestLapin->hide();*/
    ui->label->hide();
    PageLivraison = new livraison(this, idUser);
    ui->label->hide();
    PageLivraison->setGeometry(9,30,1330,650);
    PageLivraison->changerOgletActif(0);
    PageLivraison->show();
}

void Accueil::action_ConsulterPorcs_triggered()
{
    PageLivraison->hide();
     /*PageRamassage->hide();*/
    PageGestReproduction->hide();
    //PageGestPorcs->hide();
    /*PageVolaile->hide();
    PageGestLapin->hide();*/
    ui->label->hide();
    //PageGestPorcs = new Gestporcs(this, idUser);
    PageLivraison = new livraison(this, idUser);
    ui->label->hide();
    PageLivraison->setGeometry(9,30,1330,650);
    PageLivraison->changerOgletActif(1);
    PageLivraison->show();
}

void Accueil::action_enregistrementVolaille_triggered()
{
    /*PageLivraison->hide();
    PageRamassage->hide();
    PageGestReproduction->hide();
    PageGestPorcs->hide();
    PageVolaile->hide();
    PageGestLapin->hide();
    ui->label->hide();
    PageGestPorcs = new Gestporcs(this, idUser);
    ui->label->hide();
    PageVolaile->setGeometry(9,30,1330,650);
    PageVolaile->changerOgletActif(0);
    PageVolaile->show();*/
}

void Accueil::action_ListeVolaille_triggered()
{
   /* PageLivraison->hide();
    PageRamassage->hide();
    PageGestReproduction->hide();
    PageGestPorcs->hide();
    PageVolaile->hide();
    PageGestLapin->hide();
    ui->label->hide();
    PageGestPorcs = new Gestporcs(this, idUser);
    ui->label->hide();
    PageVolaile->setGeometry(9,30,1330,650);
    PageVolaile->changerOgletActif(1);
    PageVolaile->show();*/
}

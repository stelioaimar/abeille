#ifndef GESTPORCS_H
#define GESTPORCS_H

#include <QWidget>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QMessageBox>
#include <outilsmimes.h>

namespace Ui {
class Gestporcs;
}

class Gestporcs : public QWidget
{
    Q_OBJECT

public:
    explicit Gestporcs(QWidget *parent = 0, QString currentUser="");
    ~Gestporcs();
    void changerOgletActif(int ind);

private slots:
    void afficherInfo2();

    void effacerChamps();

    void on_pushButtonEnregistrer_clicked();

    void on_comboBox_critere_currentTextChanged(const QString &arg1);

    void on_comboBox_liste_Critere_currentTextChanged(const QString &arg1);

    void on_dateEdit_fin_dateChanged(const QDate &date);

    void on_dateEdit_debut_dateChanged(const QDate &date);

    void on_pushButtonImprimerConsultation_clicked();

private:
    Ui::Gestporcs *ui;
    QSqlQuery reqInsertion,requeteConsulter;
    QSqlQueryModel *modeleCritere,*modelConsulter;


    QString idUser;
};

#endif // GESTPORCS_H

#-------------------------------------------------
#
# Project created by QtCreator 2021-05-22T19:03:30
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = abeille
TEMPLATE = app

include (C:/Users/Aimar/Documents/MIMES/abeille/QtXlsxWriter/src/xlsx/qtxlsx.pri)

SOURCES += main.cpp \
    accueil.cpp \
    authentification.cpp \
    livraison.cpp \
    outilsmimes.cpp \
    gestreproduction.cpp \
    gestporcs.cpp

HEADERS  += \
    accueil.h \
    authentification.h \
    connection.h \
    livraison.h \
    outilsmimes.h \
    gestreproduction.h \
    gestporcs.h

FORMS    += \
    accueil.ui \
    authentification.ui \
    livraison.ui \
    gestreproduction.ui \
    gestporcs.ui

RESOURCES += \
    images.qrc


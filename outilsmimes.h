#ifndef OUTILSMIMES_H
#define OUTILSMIMES_H

#include <QString>
#include <QtSql>
#include <QTableView>
#include <QWidget>
#include <QDesktopWidget>
#include <QHeaderView>
#include <QComboBox>
#include <QLineEdit>
#include <QCompleter>
#include <QPrinter>
#include <QPainter>
#include <QPrintPreviewDialog>
#include <QPrintPreviewWidget>
#include <QTextEdit>
#include <QTextDocument>
#include <QTextTable>
#include <QTextCursor>
#include <QTextBrowser>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QToolButton>
#include <QMessageBox>
#include <QInputDialog>
#include <QFile>
#include <QTextStream>
#include <QTextCodec>
#include <QObject>
#include <QWidget>
#include <QFileDialog>
#include "xlsxdocument.h"
#include "xlsxcellrange.h"
#include "xlsxcell.h"
#include "xlsxformat.h"
#include "xlsxcellformula.h"
#include "xlsxworksheet.h"
#include <QProcess>

class OPLComboBox : public QComboBox
{
    Q_OBJECT

  public:
      explicit OPLComboBox(){}
      //~OPLComboBox(){}
  private slots:
      void changer_ListeFilieres(QString cycle)
      {
       QSqlQueryModel *model = new QSqlQueryModel;
       model->setQuery("SELECT Libelle FROM filiere JOIN cycle "
                       "ON cycle.idCycle = filiere.idCycle WHERE libelleCycle = '"+cycle+"';");
       this->setModel(model);
      }

      void changer_ListeAnnees(QString cycle)
      {
       this->clear();
       if(cycle == "BTS" || cycle == "Master" || cycle == "Doctorat"){
          this->addItem("1ère année");
          this->addItem("2ème année");
         }
       else if(cycle == "Licence"){
               this->addItem("1ère année");
               this->addItem("2ème année");
               this->addItem("3ème année");
            }
      }
};

class OutilsMimes
{
public:
    OutilsMimes();
    static void customizeTableWidget(QTableWidget* twidget);
    static void customizeTableWidget2(QTableWidget* twidget);
    static void customizeTableView(QTableView* tview);
    static void customComboBox(QComboBox* cbox, QSqlQuery query);
    static void customLineEdit(QLineEdit* lineEdit, QSqlQuery query);
    static void customizeToolButton(QToolButton* toolButton, QString textButton, QString iconButton);
    static QDate stringToDate(QString dateString); //Permet de convertir dateString en QDate
    static void afficherApercuImpression(QSqlQueryModel* model, QString titreTable,
                                         QString piedPage, QPrinter *printer );
    static void afficherApercuImpression(QTableWidget* tableWidget, QString titreTable,
                                         QString piedPage, QPrinter *printer,QString signataire );
    static void creerFichierExcel(QSqlQueryModel* model, QString titreTable,
                                  QString piedPage);
};

#endif // OUTILSMIMES_H

#ifndef AUTHENTIFICATION_H
#define AUTHENTIFICATION_H
#include "accueil.h"
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QMessageBox>

#include <QWidget>

namespace Ui {
class authentification;
}

class authentification : public QWidget
{
    Q_OBJECT

public:
    explicit authentification(QWidget *parent = 0);
    ~authentification();

private slots:
    void on_btn_connection_clicked();

    void on_btn_oeil_pressed();

    void on_btn_oeil_released();

    void on_btn_quitter_clicked();

private:
    Ui::authentification *ui;
    QString login;
    QString identifiantUser;
    QSqlQuery req_connection;
    QSqlRecord Record;

};

#endif // AUTHENTIFICATION_H

    #ifndef ACCUEIL_H
    #define ACCUEIL_H
    #include <QMainWindow>
    #include "authentification.h"
    #include <QToolButton>
    #include <QWidgetAction>
    #include <QGraphicsColorizeEffect>
    #include "QToolBar"
    #include "outilsmimes.h"
    #include "gestreproduction.h"
    #include "QMenu"
    #include "gestporcs.h"
    #include "livraison.h"



    namespace Ui {
    class Accueil;
    }

    class Accueil : public QMainWindow
    {
        Q_OBJECT

    public:
        explicit Accueil(QWidget *parent = 0, QString currentUser = "0");
        ~Accueil();
    private slots:
        void on_actionOPERATION_triggered();
        void on_actionCONSULTATION_triggered();
        void on_actionOPERat_triggered();
        void on_actionEnreFacture_triggered();
        void on_actionCONSUL_triggered();
        void on_actionAJOUTER_UTILISATEUR_triggered();
        void on_actionQUITTER_triggered();
        void on_actionRECHERCHE_triggered();
        void on_actionDECONNECTE_triggered();
        void on_actionParametre_archive_triggered();
        void on_actionParametre_operation_triggered();
        // actions creer pour le menu livraison
        void on_action_enregistrement_gestrepro_triggered();
        void on_action_listes_gestrepro_triggered();

        void afficherAccueil();

        void on_actionConsultation_de_l_archive_triggered();

        void on_actionConsulter_archives_triggered();

        void action_enregistrementPorcs_triggered();
        void action_ConsulterPorcs_triggered();
        void action_enregistrementVolaille_triggered();
        void action_ListeVolaille_triggered();


    private:
        Ui::Accueil *ui;
        GestReproduction *PageGestReproduction;
         livraison *PageLivraison;
        /*Ramassage *PageRamassage;
        Volaille *PageVolaile;
        GestLapin *PageGestLapin;
        Gestporcs *PageGestPorcs;
        */

       /* QMenu *menu_GestReproduction, *menu_Archive, *menu_Facture,*menu_Ramassage,*menu_Livraison,*menu_GestPorcs,
                *menu_Volaille;*/
         QMenu *menu_GestReproduction,*menu_GestPorcs;
        QAction *actionAccueil,*action_enregistrement_consultation,
        *action_enregistrement_gestrepro,*action_listes_gestrepro,*action_enregistrementPorcs,*action_ConsulterPorcs,
        *action_listes_consultation,*action_listes_facture,*action_ParametreArchive,
        *action_enregistrement_volaille , *action_listes_volaille;

        //QAction *action_enregistrement_ramassage,*action_listes_ramassage;
        QAction *action_enregistrement_livraison,*action_listes_livraison;

        QWidgetAction *toolButtonAction, *toolButtonAction_1, *toolButtonAction_2,
        *toolButtonAction_3, *toolButtonAction_4, *toolButtonAction_5, *toolButtonAction_6,
        *toolButtonAction_7;
        QAction *action_Aide, *action_A_Propos, *actionDeconnexion, *actionQuitter,
                *action_Operation, *action_Utilisateurs, *action_ChangePassword,*actionParametre;
        QToolBar *mainToolBar;
        bool deconnecter;
        QString idUser;

    };

    #endif // ACCUEIL_H

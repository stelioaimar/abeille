#ifndef LIVRAISON_H
#define LIVRAISON_H

#include <QWidget>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlRecord>
#include "outilsmimes.h"

namespace Ui {
class livraison;
}

class livraison : public QWidget
{
    Q_OBJECT

public:
    explicit livraison(QWidget *parent = 0, QString currentUser="");
    ~livraison();
    void changerOgletActif(int ind);

private slots:
    void on_pushButtonEnregistrer_clicked();
    void afficherInfo();
    void afficherInfo2();
    void effacerChamps();

    void on_tableWidget_Enreg_doubleClicked(const QModelIndex &index);

    void on_pushButtonModifier_clicked();

    void on_comboBox_critere_currentTextChanged(const QString &arg1);

    void on_comboBox_liste_Critere_currentTextChanged(const QString &arg1);

    void on_dateEdit_debut_userDateChanged(const QDate &date);

    void on_dateEdit_fin_dateChanged(const QDate &date);
    void affich();

    void on_pushButtonImprimerConsultation_clicked();
    void afficherApercuListe(QPrinter *printer);
    //void afficherApercuListeTotal(QPrinter *printer);

    void on_pushButton_Ajouter_clicked();

    void on_pushButton_Retirer_clicked();

    void on_pushButtonImprimerfiche_clicked();

    void afficherApercuFiche(QPrinter *printer);


private:
    Ui::livraison *ui;

    QSqlQuery requeteInsertion,requeteSelection,requeteConsulter,requeteConsulter2,reqinit,reqentre;
    QSqlQueryModel *modeleSelection,*modeleCritere,*modelConsulter,*modelConsulter2,*modeleInit,*modeleEntree;

    QString idUser;
    int numero;
};

#endif // LIVRAISON_H

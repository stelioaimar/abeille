#include "authentification.h"
#include "ui_authentification.h"

authentification::authentification(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::authentification)
{
   ui->setupUi(this);
   ui->lineEdit_identifiant->setFocus();
   ui->lineEdit_motDePasse->setEchoMode(QLineEdit::Password);

}

authentification::~authentification()
{
    delete ui;
}

void authentification::on_btn_connection_clicked()
{
    QString identifiant,motDePasse;
    identifiant = ui->lineEdit_identifiant->text();
    motDePasse = ui->lineEdit_motDePasse->text();
    req_connection.exec("SELECT COUNT(*), idUser, login, password FROM user WHERE login ='"+identifiant+"' AND password='"+motDePasse+"'");
    QSqlQueryModel *mod=new QSqlQueryModel(this);
    mod->setQuery(req_connection);
    Record=mod->record(0);

    if(Record.value(0).toInt()<1)
    {
        QMessageBox::critical(this,"Erreur de connection","L'identifiant ou le mot de passe est incorrect");
    }
    else if(Record.value(0).toInt()>1)
    {
        QMessageBox::critical(this,"Erreur duplication","Veuillez contacter votre concepteur");
    }
    else if(Record.value(0).toInt()==1)
        {
            if(Record.value(2).toString()==identifiant and Record.value(3).toString()==motDePasse)
            {
                   Accueil *PageAccueil = new Accueil(0,Record.value(1).toString());
                this->hide();
                PageAccueil->show();
             }

            else
                {
                QMessageBox::critical(this,"Erreur Authentification","L'identifiant ou le mot de passe est incorrect");
                }

        }

 }


void authentification::on_btn_oeil_released()
{
    ui->lineEdit_motDePasse->setEchoMode(QLineEdit::Password);
}
void authentification::on_btn_oeil_pressed()
{
    ui->lineEdit_motDePasse->setEchoMode(QLineEdit::Normal);
}

void authentification::on_btn_quitter_clicked()
{
    close();
}

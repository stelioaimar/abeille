#include "livraison.h"
#include "ui_livraison.h"

livraison::livraison(QWidget *parent, QString currentUser) :
    QWidget(parent),
    ui(new Ui::livraison)
{
    ui->setupUi(this);
    idUser=currentUser;
    ui->dateEdit_MiseBas->setDate(QDate::currentDate());
    ui->dateEdit_RetourChaleur->setDate(QDate::currentDate());
    ui->dateEdit_visite->setDate(QDate::currentDate());
    ui->dateEdit_Sevrage->setDate(QDate::currentDate());
    //ui->dateEdit_Enreg->hide();
    ui->dateEdit_debut->hide();
    ui->dateEdit_fin->hide();
    ui->label_debut->hide();
    ui->label_fin->hide();
    //ui->dateEdit_debut->setDate(QDate::currentDate());
   //ui->dateEdit_fin->setDate(QDate::currentDate());

    modeleSelection = new QSqlQueryModel();
    modeleCritere = new QSqlQueryModel();
    modelConsulter = new QSqlQueryModel();
    modelConsulter2 = new QSqlQueryModel();
    modeleInit = new QSqlQueryModel();
    modeleEntree = new QSqlQueryModel();
    afficherInfo();
    afficherInfo2();
    affich();
    ui->dateEdit_MiseBas->hide();
    ui->dateEdit_RetourChaleur->hide();
    ui->dateEdit_Sevrage->hide();
    ui->spinBoxPorte->hide();
    ui->label_nbr_Pj_6->hide();
    ui->label_nbr_Pj_5->hide();
    ui->label_nbr_Pj_8->hide();
    ui->label_objet_5->hide();
    //ui->tableWidget_total->hide();
    ui->tableWidget_ActionEffectuees->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    requeteSelection.prepare("select DISTINCT numRuche FROM ruche");
    OutilsMimes::customComboBox(ui->comboBoxNumRuche,requeteSelection);
}

livraison::~livraison()
{
    delete ui;
}
void livraison::changerOgletActif(int ind)
{
    ui->tabWidget->setCurrentIndex(ind);
}

void livraison::on_pushButtonEnregistrer_clicked()
{
    if(ui->comboBoxNumRuche->currentText().isEmpty()){
            QMessageBox::critical(0,"Enregistrement","Veuillez remplir tous les champs obligatoires SVP !");
    }else {
    requeteInsertion.prepare("INSERT INTO visite(idRuche,idTypeV, dateVisite, heureVisite, "
                             "constat, recommandation, QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis, idUser, dateEnregistrement ) VALUES "
                             "((select idRuche from ruche where numRuche=?),"
                             "(select idTypeV from typeVisite where libelleTypeV=?),"
                             "'"+ui->dateEdit_visite->date().toString("yyyy-MM-dd")+"',"
                             "'"+ui->timeEdit_visite->time().toString("HH:mm:ss")+"',"
                             "?,?,?,?,?,?,?,"
                             "'"+QDate::currentDate().toString("yyyy-MM-dd")+"')");

      requeteInsertion.bindValue(0,ui->comboBoxNumRuche->currentText());
      requeteInsertion.bindValue(1,ui->comboBoxtypeVisite->currentText());
      requeteInsertion.bindValue(2,ui->lineEdit_constatVisite->text());
      requeteInsertion.bindValue(3,ui->lineEdit_recommandations->text());
      requeteInsertion.bindValue(4,ui->doubleSpinBox_litreRecoltes->value());
      requeteInsertion.bindValue(5,ui->doubleSpinBox_kilosCire->value());
      requeteInsertion.bindValue(6,ui->doubleSpinBox_qtePollen->value());
      requeteInsertion.bindValue(7,ui->doubleSpinBox_kilosPropolis->value());
      requeteInsertion.bindValue(8,idUser.toInt());

      if(!requeteInsertion.exec())
          QMessageBox::warning(this,"Erreur insertion visite",requeteInsertion.lastError().text());

      //pour l'enregistrement des actions de la visite
      QSqlQuery query,requeteAction;
      query.prepare("INSERT INTO actionVisite(idVisite, idAction) VALUES (:idVisite, :idAction); ");
         QString actions, idAction;
         for(int i=0; i<ui->tableWidget_ActionEffectuees->rowCount(); i++){

             QObject *obj2 = new QComboBox;
             obj2 = ui->tableWidget_ActionEffectuees->cellWidget(i,0);
             QComboBox *comboBox_Action = qobject_cast<QComboBox *>(obj2);
             actions = comboBox_Action->currentText();

             requeteAction.exec("SELECT idAction FROM actionEffectuee WHERE LibelleAction = '"+actions+"'");
             while(requeteAction.next())
                 idAction = requeteAction.value(0).toString();

             query.bindValue(0, requeteInsertion.lastInsertId().toString());
             query.bindValue(1, idAction);
             if(!query.exec())
                 QMessageBox::warning(this,"Erreur insertion action visite",query.lastError().text());
                 //ui->tableWidget_ECUs->item(i, 0)->setText("!Erreur");
         }
      afficherInfo();
      effacerChamps();
      //ui->tableWidget_ActionEffectuees->setRowCount(0);
      //this->on_pushButton_AjouterFiliere_clicked();
      //OutilsMimes::customizeTableWidget(ui->tableWidget_Filieres);
      }
}

void livraison::afficherInfo()
{
    requeteSelection.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                             "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                             "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                             "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                             "visite.idRuche=ruche.idRuche AND "
                             "visite.idTypeV=typeVisite.idTypeV AND "
                             "visite.idVisite=actionVisite.idVisite AND "
                             "actionVisite.idAction=actionEffectuee.idAction AND "
                             "dateEnregistrement ='"+QDate::currentDate().toString("yyyy-MM-dd")+"'");
         if (!requeteSelection.exec())
        QMessageBox::information(this,"Erreur selection visite",requeteSelection.lastError().text());
        modeleSelection->setQuery(requeteSelection);
        ui->tableWidget_Enreg->setColumnCount(11);
        QStringList entete;
        entete<<"NUMERO\nRUCHE"<<"DATE\nVISITE"<<"HEURE\nVISITE"<<"TYPE\nVISITE"<<"ACTIONS\nEFFECTUEES"<<"LITRES\nRECOLTES"<<"KILOS\nCIRES"
         <<"QUANTITE\nPOLLEN"<<"KILOS\nPROPOLIS"<<"CONSTAT\nVISITE"<<"RECOM-\nMANDATIONS";
        ui->tableWidget_Enreg->setHorizontalHeaderLabels(entete);
         for (int i=0; i<requeteSelection.size(); i++)
         {
             ui->tableWidget_Enreg->setRowCount(i+1);
             QTableWidgetItem *itemNumRuch = new QTableWidgetItem(modeleSelection->record(i).value(0).toString());
             itemNumRuch->setFlags(Qt::ItemIsEnabled);
             itemNumRuch->setTextAlignment(Qt::AlignCenter);
             itemNumRuch->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemDateVisite = new QTableWidgetItem(modeleSelection->record(i).value(1).toString());
             itemDateVisite->setFlags(Qt::ItemIsEnabled);
             itemDateVisite->setTextAlignment(Qt::AlignCenter);
             itemDateVisite->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemHeureVisite = new QTableWidgetItem(modeleSelection->record(i).value(2).toString());
             itemHeureVisite->setFlags(Qt::ItemIsEnabled);
             itemHeureVisite->setTextAlignment(Qt::AlignCenter);
             itemHeureVisite->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemTypeVisite = new QTableWidgetItem(modeleSelection->record(i).value(3).toString());
             itemTypeVisite->setFlags(Qt::ItemIsEnabled);
             itemTypeVisite->setTextAlignment(Qt::AlignCenter);
             itemTypeVisite->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemActions = new QTableWidgetItem(modeleSelection->record(i).value(4).toString());
             itemActions->setFlags(Qt::ItemIsEnabled);
             itemActions->setTextAlignment(Qt::AlignCenter);
             itemActions->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemQteRecolte = new QTableWidgetItem(modeleSelection->record(i).value(5).toString());
             itemQteRecolte->setFlags(Qt::ItemIsEnabled);
             itemQteRecolte->setTextAlignment(Qt::AlignCenter);
             itemQteRecolte->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemQteCire = new QTableWidgetItem(modeleSelection->record(i).value(6).toString());
             itemQteCire->setFlags(Qt::ItemIsEnabled);
             itemQteCire->setTextAlignment(Qt::AlignCenter);
             itemQteCire->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemQtePollen = new QTableWidgetItem(modeleSelection->record(i).value(7).toString());
             itemQtePollen->setFlags(Qt::ItemIsEnabled);
             itemQtePollen->setTextAlignment(Qt::AlignCenter);
             itemQtePollen->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemQtePropolis = new QTableWidgetItem(modeleSelection->record(i).value(8).toString());
             itemQtePropolis->setFlags(Qt::ItemIsEnabled);
             itemQtePropolis->setTextAlignment(Qt::AlignCenter);
             itemQtePropolis->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemConstat = new QTableWidgetItem(modeleSelection->record(i).value(9).toString());
             itemConstat->setFlags(Qt::ItemIsEnabled);
             itemConstat->setTextAlignment(Qt::AlignCenter);
             itemConstat->setForeground(QBrush(QColor(255,255,255)));
             QTableWidgetItem *itemRecommandation = new QTableWidgetItem(modeleSelection->record(i).value(10).toString());
             itemRecommandation->setFlags(Qt::ItemIsEnabled);
             itemRecommandation->setTextAlignment(Qt::AlignCenter);
             itemRecommandation->setForeground(QBrush(QColor(255,255,255)));

             ui->tableWidget_Enreg->setItem(i,0,itemNumRuch);
             ui->tableWidget_Enreg->setItem(i,1,itemDateVisite);
             ui->tableWidget_Enreg->setItem(i,2,itemHeureVisite);
             ui->tableWidget_Enreg->setItem(i,3,itemTypeVisite);
             ui->tableWidget_Enreg->setItem(i,4,itemActions);
             ui->tableWidget_Enreg->setItem(i,5,itemQteRecolte);
             ui->tableWidget_Enreg->setItem(i,6,itemQteCire);
             ui->tableWidget_Enreg->setItem(i,7,itemQtePollen);
             ui->tableWidget_Enreg->setItem(i,8,itemQtePropolis);
             ui->tableWidget_Enreg->setItem(i,9,itemConstat);
             ui->tableWidget_Enreg->setItem(i,10,itemRecommandation);
         }
         OutilsMimes::customizeTableWidget(ui->tableWidget_Enreg);
         ui->pushButtonEnregistrer->setEnabled(true);
         ui->pushButtonModifier->setDisabled(true);
}

void livraison::effacerChamps()
{
    /*ui->spinBox_pltCasseLivre->clear();
    ui->spinBox_pltNorLivre->clear();
    //ui->spinBox_uom->clear();
    //ui->spinBox_pom->clear();
    //ui->spinBox_Entree->clear();
    ui->textEdit_observation->clear();*/
    ui->lineEdit_constatVisite->clear();
    ui->comboBoxtypeVisite->clearEditText();
    ui->comboBoxNumRuche->clearEditText();
    ui->spinBoxPorte->clear();
    ui->doubleSpinBox_litreRecoltes->clear();

}

void livraison::on_tableWidget_Enreg_doubleClicked(const QModelIndex &index)
{
    int ligne = index.row();
    ui->comboBoxNumRuche->setCurrentText(modeleSelection->record(ligne).value(0).toString());
    ui->comboBoxtypeVisite->setCurrentText(modeleSelection->record(ligne).value(1).toString());
    ui->dateEdit_visite->setDate(modeleSelection->record(ligne).value(2).toDate());
    ui->dateEdit_RetourChaleur->setDate(modeleSelection->record(ligne).value(3).toDate());
    ui->dateEdit_MiseBas->setDate(modeleSelection->record(ligne).value(4).toDate());
    ui->spinBoxPorte->setValue(modeleSelection->record(ligne).value(5).toInt());
    ui->dateEdit_Sevrage->setDate(modeleSelection->record(ligne).value(6).toDate());
    ui->doubleSpinBox_litreRecoltes->setValue(modeleSelection->record(ligne).value(7).toDouble());
    ui->lineEdit_constatVisite->setText(modeleSelection->record(ligne).value(8).toString());

   /* ui->lineEdit->setText(modeleSelection->record(ligne).value(11).toString());
    ui->textEdit_observation->setText(modeleSelection->record(ligne).value(12).toString());*/

    //ui->textEdit_Ramassage2->setText(modeleSelection->record(ligne).value(10).toString());
    numero=modeleSelection->record(ligne).value(9).toInt();
    ui->pushButtonModifier->setEnabled(true);
    ui->pushButtonEnregistrer->setDisabled(true);
}

void livraison::on_pushButtonModifier_clicked()
{

    reqinit.exec("SELECT pltMagasin, unitMagasin from livraison  order by idLivraison desc limit 1,1");
    reqentre.exec("SELECT nbPlOeuf, nbUnitOeuf from ramassage  WHERE dateRamassage= '"+QDate::currentDate().toString("yyyy-MM-dd")+"' ");
    modeleInit->setQuery(reqinit);
    modeleEntree->setQuery(reqentre);
    if(!reqinit.exec())
        QMessageBox::warning(this,"Erreur insertion livraison",reqinit.lastError().text());
    if(!reqentre.exec())
        QMessageBox::warning(this,"Erreur insertion livraison",reqentre.lastError().text());


    /*if(ui->lineEdit->text().isEmpty()){
            QMessageBox::critical(0,"Modification","Veuillez remplir tous les champs obligatoires SVP !");
    }else {
                requeteInsertion.prepare("UPDATE livraison SET dateLivraison='"+ui->dateEdit_Livraison->date().toString("yyyy-MM-dd")+"',"
                                         " pltInitiale=?, uniteInitiale=?, pltEntree=?, uniteEntree=?, pltDisponible=?, "
                                         " uniteDisponiible=?, nbPltNormLivre=?, nbPltCasseLivre=?, pltMagasin=?, unitMagasin=?, receveur=?,"
                                          "observation=?, idUser=? WHERE idLivraison='"+QString::number(numero)+"'");*/

                /*requeteInsertion.bindValue(0,ui->spinBox_stockInit->value());
                requeteInsertion.bindValue(1,ui->spinBox_Entree->value());
                requeteInsertion.bindValue(2,ui->spinBox_pnl->value());
                requeteInsertion.bindValue(3,ui->spinBox_pcl->value());
                int res=ui->spinBox_Entree->value()-(ui->spinBox_pcl->value()+ui->spinBox_pnl->value());
                requeteInsertion.bindValue(4,res);
                requeteInsertion.bindValue(5,ui->spinBox_pom->value());
               // requeteInsertion.bindValue(6,ui->spinBox_6->value());
                requeteInsertion.bindValue(6,ui->lineEdit->text());
                requeteInsertion.bindValue(7,ui->textEdit_observation->toPlainText());
                requeteInsertion.bindValue(8,idUser.toInt());
                requeteInsertion.bindValue(9,ui->spinBox_uom->value());*/

                requeteInsertion.bindValue(0,modeleInit->record(0).value(0).toInt());
                requeteInsertion.bindValue(1,modeleInit->record(0).value(1).toInt());
                requeteInsertion.bindValue(2,modeleEntree->record(0).value(0).toInt());
                requeteInsertion.bindValue(3,modeleEntree->record(0).value(1).toInt());//ddd
                //int val=200;
                //int result=ui->spinBox_Entree->value()-(ui->spinBox_pcl->value()+ui->spinBox_pnl->value());
                int resultat=0,unitRestant=0;
                if ((modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())>=30)
                {
                    resultat = (modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())/30;
                    unitRestant=(modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt())-(resultat*30);
                }
                else
                    unitRestant=(modeleInit->record(0).value(1).toInt()+modeleEntree->record(0).value(1).toInt());
               /* requeteInsertion.bindValue(4,modeleInit->record(0).value(0).toInt()+ modeleEntree->record(0).value(0).toInt()+resultat);
                requeteInsertion.bindValue(5,unitRestant);
                requeteInsertion.bindValue(6,ui->spinBox_pltNorLivre->value());
                requeteInsertion.bindValue(7,ui->spinBox_pltCasseLivre->value());
                requeteInsertion.bindValue(8,(modeleInit->record(0).value(0).toInt()+ modeleEntree->record(0).value(0).toInt()+resultat)-(ui->spinBox_pltNorLivre->value()+ui->spinBox_pltCasseLivre->value()));
                requeteInsertion.bindValue(9,unitRestant);
                requeteInsertion.bindValue(10,ui->lineEdit->text());
                requeteInsertion.bindValue(11,ui->textEdit_observation->toPlainText());*/
                requeteInsertion.bindValue(12,idUser.toInt());

                if(!requeteInsertion.exec())
                    QMessageBox::warning(this,"Erreur modification reproduction",requeteInsertion.lastError().text());
                afficherInfo();
                effacerChamps();
  }


void livraison::on_comboBox_critere_currentTextChanged(const QString &arg1)
{
    if(arg1=="Période" or arg1=="Date enregistrement")
   {   ui->dateEdit_debut->setDate(QDate::currentDate());
       ui->dateEdit_fin->setDate(QDate::currentDate());
       ui->label_debut->show();
       ui->dateEdit_debut->show();
       ui->label_fin->show();
       ui->dateEdit_fin->show();
       ui->comboBox_liste_Critere->hide();
       ui->label_debut->setGeometry(300,10,110,31);
       ui->dateEdit_debut->setGeometry(400,10,110,31);
       ui->label_fin->setGeometry(520,10,110,31);
       ui->dateEdit_fin->setGeometry(600,10,110,31);
   }
   else
   {
       ui->comboBox_liste_Critere->clear();
       ui->comboBox_liste_Critere->addItem("");
       ui->comboBox_liste_Critere->show();
       ui->label_debut->hide();
       ui->label_fin->hide();
       ui->dateEdit_debut->hide();
       ui->dateEdit_fin->hide();
       ui->tableWidgetConsult->setRowCount(0);
        if(arg1=="Numéro de la ruche")
       {
          requeteConsulter.exec("SELECT DISTINCT numRuche FROM ruche");
       }
        else if(arg1=="Type de visite")
       {
          requeteConsulter.exec("SELECT DISTINCT libelleTypeV FROM typeVisite");
       }
       else if(arg1=="Actions effectuées")
       {
          requeteConsulter.exec("SELECT DISTINCT libelleAction FROM actionEffectuee");
       }
       else if(arg1=="Nombre de litres récoltés")
       {
          requeteConsulter.exec("SELECT DISTINCT QteLitreRecolte FROM visite");
       }
       else if(arg1=="Nombre de kilos de cires")
       {
          requeteConsulter.exec("SELECT DISTINCT QteKiloCire FROM visite");
       }
       else if(arg1=="Quantité de pollen")
       {
          requeteConsulter.exec("SELECT DISTINCT QtePollen FROM visite");
       }
        else if(arg1=="Nombre de kilos de propolis")
        {
           requeteConsulter.exec("SELECT DISTINCT QteKiloPropolis FROM visite");
        }
        else if(arg1=="Constat de visite")
        {
           requeteConsulter.exec("SELECT DISTINCT constat FROM visite");
        }
        else if(arg1=="Recommandations")
        {
           requeteConsulter.exec("SELECT DISTINCT recommandation FROM visite");
        }

       modeleCritere->setQuery(requeteConsulter);
       int i = 0;
       while (requeteConsulter.next())
       {
           ui->comboBox_liste_Critere->addItem(modeleCritere->record(i).value(0).toString());
           i++;
       }
   }
}

void livraison::on_comboBox_liste_Critere_currentTextChanged(const QString &arg1)
{
    QString critere_Choix;
    critere_Choix=ui->comboBox_critere->currentText();
    if(critere_Choix=="Numéro de la ruche")
    {
        requeteConsulter.prepare("SELECT distinct numRuche, dateVisite, heureVisite, libelleTypeV, "
                                 "libelleAction, "
                                 "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                 "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                 "visite.idRuche=ruche.idRuche AND "
                                 "visite.idTypeV=typeVisite.idTypeV AND "
                                 "visite.idVisite=actionVisite.idVisite AND "
                                 "actionVisite.idAction=actionEffectuee.idAction AND numRuche =? ");

       /* requeteConsulter2.prepare("SELECT numRuche,"
                                 "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees "
                                 " FROM "
                                  "visite,actionVisite,actionEffectuee,ruche WHERE "
                                  "visite.idRuche=ruche.idRuche AND "
                                 "visite.idVisite=actionVisite.idVisite AND "
                                 "actionVisite.idAction=actionEffectuee.idAction AND numRuche =? ");
        requeteConsulter2.bindValue(0,arg1);*/
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
        /*if(!requeteConsulter2.exec())
            QMessageBox::critical(this,"erreur sélection information2",requeteConsulter2.lastError().text());*/
    }
    else if(critere_Choix=="Type de visite")
     {
        requeteConsulter.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                                 "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                                 "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                 "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                 "visite.idRuche=ruche.idRuche AND "
                                 "visite.idTypeV=typeVisite.idTypeV AND "
                                 "visite.idVisite=actionVisite.idVisite AND "
                                 "actionVisite.idAction=actionEffectuee.idAction AND libelleTypeV =? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if(critere_Choix=="Actions effectuées")
     {
        requeteConsulter.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                                 "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                                 "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                 "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                 "visite.idRuche=ruche.idRuche AND "
                                 "visite.idTypeV=typeVisite.idTypeV AND "
                                 "visite.idVisite=actionVisite.idVisite AND "
                                 "actionVisite.idAction=actionEffectuee.idAction AND libelleAction =? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if(critere_Choix=="Nombre de litres récoltés")
     {
        requeteConsulter.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                                 "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                                 "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                 "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                 "visite.idRuche=ruche.idRuche AND "
                                 "visite.idTypeV=typeVisite.idTypeV AND "
                                 "visite.idVisite=actionVisite.idVisite AND "
                                 "actionVisite.idAction=actionEffectuee.idAction AND QteLitreRecolte =? ");
        requeteConsulter.bindValue(0,arg1);
        if(!requeteConsulter.exec())
            QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    else if(critere_Choix=="Nombre de kilos de cires")
    {
         requeteConsulter.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                                  "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                                  "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                  "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                  "visite.idRuche=ruche.idRuche AND "
                                  "visite.idTypeV=typeVisite.idTypeV AND "
                                  "visite.idVisite=actionVisite.idVisite AND "
                                  "actionVisite.idAction=actionEffectuee.idAction AND QteKiloCire =? ");
          requeteConsulter.bindValue(0,arg1);
          if(!requeteConsulter.exec())
               QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
     }
    else if(critere_Choix=="Quantité de pollen")
    {
         requeteConsulter.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                                  "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                                  "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                  "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                  "visite.idRuche=ruche.idRuche AND "
                                  "visite.idTypeV=typeVisite.idTypeV AND "
                                  "visite.idVisite=actionVisite.idVisite AND "
                                  "actionVisite.idAction=actionEffectuee.idAction AND QtePollen =? ");
         requeteConsulter.bindValue(0,arg1);
          if(!requeteConsulter.exec())
               QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
     }
    else if(critere_Choix=="Nombre de kilos de propolis")
    {
         requeteConsulter.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                                  "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                                  "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                  "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                  "visite.idRuche=ruche.idRuche AND "
                                  "visite.idTypeV=typeVisite.idTypeV AND "
                                  "visite.idVisite=actionVisite.idVisite AND "
                                  "actionVisite.idAction=actionEffectuee.idAction AND QteKiloPropolis =? ");
         requeteConsulter.bindValue(0,arg1);
          if(!requeteConsulter.exec())
               QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
     }
    else if(critere_Choix=="Constat de visite")
    {
         requeteConsulter.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                                  "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                                  "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                  "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                  "visite.idRuche=ruche.idRuche AND "
                                  "visite.idTypeV=typeVisite.idTypeV AND "
                                  "visite.idVisite=actionVisite.idVisite AND "
                                  "actionVisite.idAction=actionEffectuee.idAction AND constat =? ");
         requeteConsulter.bindValue(0,arg1);
          if(!requeteConsulter.exec())
               QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
     }
    else if(critere_Choix=="Recommandations")
    {
         requeteConsulter.prepare("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                                  "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                                  "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                                  "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                                  "visite.idRuche=ruche.idRuche AND "
                                  "visite.idTypeV=typeVisite.idTypeV AND "
                                  "visite.idVisite=actionVisite.idVisite AND "
                                  "actionVisite.idAction=actionEffectuee.idAction AND recommandation =? ");
         requeteConsulter.bindValue(0,arg1);
          if(!requeteConsulter.exec())
               QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
     }


    modelConsulter->setQuery(requeteConsulter);
     afficherInfo2();
     affich();
}
void livraison::afficherInfo2()
{
    ui->tableWidgetConsult->setColumnCount(11);
    QStringList entete;
    entete<<"NUMERO\nRUCHE"<<"DATE\nVISITE"<<"HEURE\nVISITE"<<"TYPE\nVISITE"<<"ACTIONS\nEFFECTUEES"<<"LITRES\nRECOLTES"<<"KILOS\nCIRES"
     <<"QUANTITE\nPOLLEN"<<"KILOS\nPROPOLIS"<<"CONSTAT\nVISITE"<<"RECOM-\nMANDATIONS";
    if(requeteConsulter.size()==0){
        ui->tableWidgetConsult->clear();
        ui->tableWidgetConsult->setRowCount(0);
        //QMessageBox::critical(this,"erreur sélection information",requeteConsulter.lastError().text());
    }
    ui->tableWidgetConsult->setHorizontalHeaderLabels(entete);
    //Entete des totaux
    QTableWidgetItem *itemeTotalLitreRecoltes = new QTableWidgetItem("");
    itemeTotalLitreRecoltes->setFlags(Qt::ItemIsEnabled);
    itemeTotalLitreRecoltes->setTextAlignment(Qt::AlignCenter);
    itemeTotalLitreRecoltes->setForeground(QBrush(QColor(255,255,255)));
    QTableWidgetItem *itemeTotalKiloCire = new QTableWidgetItem("");
    itemeTotalKiloCire->setFlags(Qt::ItemIsEnabled);
    itemeTotalKiloCire->setTextAlignment(Qt::AlignCenter);
    itemeTotalKiloCire->setForeground(QBrush(QColor(255,255,255)));
    QTableWidgetItem *itemeTotalQtePollen = new QTableWidgetItem("");
    itemeTotalQtePollen->setFlags(Qt::ItemIsEnabled);
    itemeTotalQtePollen->setTextAlignment(Qt::AlignCenter);
    itemeTotalQtePollen->setForeground(QBrush(QColor(255,255,255)));
    QTableWidgetItem *itemeTotalKiloPropolis = new QTableWidgetItem("");
    itemeTotalKiloPropolis->setFlags(Qt::ItemIsEnabled);
    itemeTotalKiloPropolis->setTextAlignment(Qt::AlignCenter);
    itemeTotalKiloPropolis->setForeground(QBrush(QColor(255,255,255)));

    //construction des items des totaux
    ui->tableWidget_total->setItem(0,0,itemeTotalLitreRecoltes);
    ui->tableWidget_total->setItem(0,1,itemeTotalKiloCire);
    ui->tableWidget_total->setItem(0,2,itemeTotalQtePollen);
    ui->tableWidget_total->setItem(0,3,itemeTotalKiloPropolis);

     for (int i=0; i<requeteConsulter.size(); i++)
     {
         ui->tableWidgetConsult->setRowCount(i+1);

         QTableWidgetItem *itemNumRuch = new QTableWidgetItem(modelConsulter->record(i).value(0).toString());
         itemNumRuch->setFlags(Qt::ItemIsEnabled);
         itemNumRuch->setTextAlignment(Qt::AlignCenter);
         itemNumRuch->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemDateVisite = new QTableWidgetItem(modelConsulter->record(i).value(1).toString());
         itemDateVisite->setFlags(Qt::ItemIsEnabled);
         itemDateVisite->setTextAlignment(Qt::AlignCenter);
         itemDateVisite->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemHeureVisite = new QTableWidgetItem(modelConsulter->record(i).value(2).toString());
         itemHeureVisite->setFlags(Qt::ItemIsEnabled);
         itemHeureVisite->setTextAlignment(Qt::AlignCenter);
         itemHeureVisite->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemTypeVisite = new QTableWidgetItem(modelConsulter->record(i).value(3).toString());
         itemTypeVisite->setFlags(Qt::ItemIsEnabled);
         itemTypeVisite->setTextAlignment(Qt::AlignCenter);
         itemTypeVisite->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemActions = new QTableWidgetItem(modelConsulter->record(i).value(4).toString());
         itemActions->setFlags(Qt::ItemIsEnabled);
         itemActions->setTextAlignment(Qt::AlignCenter);
         itemActions->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemQteRecolte = new QTableWidgetItem(modelConsulter->record(i).value(5).toString());
         itemQteRecolte->setFlags(Qt::ItemIsEnabled);
         itemQteRecolte->setTextAlignment(Qt::AlignCenter);
         itemQteRecolte->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemQteCire = new QTableWidgetItem(modelConsulter->record(i).value(6).toString());
         itemQteCire->setFlags(Qt::ItemIsEnabled);
         itemQteCire->setTextAlignment(Qt::AlignCenter);
         itemQteCire->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemQtePollen = new QTableWidgetItem(modelConsulter->record(i).value(7).toString());
         itemQtePollen->setFlags(Qt::ItemIsEnabled);
         itemQtePollen->setTextAlignment(Qt::AlignCenter);
         itemQtePollen->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemQtePropolis = new QTableWidgetItem(modelConsulter->record(i).value(8).toString());
         itemQtePropolis->setFlags(Qt::ItemIsEnabled);
         itemQtePropolis->setTextAlignment(Qt::AlignCenter);
         itemQtePropolis->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemConstat = new QTableWidgetItem(modelConsulter->record(i).value(9).toString());
         itemConstat->setFlags(Qt::ItemIsEnabled);
         itemConstat->setTextAlignment(Qt::AlignCenter);
         itemConstat->setForeground(QBrush(QColor(255,255,255)));
         QTableWidgetItem *itemRecommandation = new QTableWidgetItem(modelConsulter->record(i).value(10).toString());
         itemRecommandation->setFlags(Qt::ItemIsEnabled);
         itemRecommandation->setTextAlignment(Qt::AlignCenter);
         itemRecommandation->setForeground(QBrush(QColor(255,255,255)));

         ui->tableWidgetConsult->setItem(i,0,itemNumRuch);
         ui->tableWidgetConsult->setItem(i,1,itemDateVisite);
         ui->tableWidgetConsult->setItem(i,2,itemHeureVisite);
         ui->tableWidgetConsult->setItem(i,3,itemTypeVisite);
         ui->tableWidgetConsult->setItem(i,4,itemActions);
         ui->tableWidgetConsult->setItem(i,5,itemQteRecolte);
         ui->tableWidgetConsult->setItem(i,6,itemQteCire);
         ui->tableWidgetConsult->setItem(i,7,itemQtePollen);
         ui->tableWidgetConsult->setItem(i,8,itemQtePropolis);
         ui->tableWidgetConsult->setItem(i,9,itemConstat);
         ui->tableWidgetConsult->setItem(i,10,itemRecommandation);

         //affichage des totaux
         ui->tableWidget_total->item(0,0)->setText(QString::number(ui->tableWidget_total->item(0,0)->text().toInt()+itemQteRecolte->text().toInt()));
         ui->tableWidget_total->item(0,1)->setText(QString::number(ui->tableWidget_total->item(0,1)->text().toInt()+itemQteCire->text().toInt()));
         ui->tableWidget_total->item(0,2)->setText(QString::number(ui->tableWidget_total->item(0,2)->text().toInt()+itemQtePollen->text().toInt()));
         ui->tableWidget_total->item(0,3)->setText(QString::number(ui->tableWidget_total->item(0,3)->text().toInt()+itemQtePropolis->text().toInt()));

     }
     OutilsMimes::customizeTableWidget(ui->tableWidgetConsult);
     ui->pushButtonEnregistrer->setEnabled(true);
     ui->pushButtonModifier->setDisabled(true);
}

void livraison::on_dateEdit_debut_userDateChanged(const QDate &date)
{
    QString critereDate= ui->comboBox_critere->currentText();
        ui->dateEdit_fin->setDate(QDate::currentDate());
        if (date > ui->dateEdit_fin->date()){
        QMessageBox::information(0,"Erreur de date","La date de debut ne peut pas être supérieur à la date de fin");
        ui->tableWidgetConsult->setRowCount(0);
        }
        else
             {
            if(critereDate=="Période"){
        requeteConsulter.exec("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                              "libelleAction, "
                              "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                              "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                              "visite.idRuche=ruche.idRuche AND "
                              "visite.idTypeV=typeVisite.idTypeV AND "
                              "visite.idVisite=actionVisite.idVisite AND "
                              "actionVisite.idAction=actionEffectuee.idAction AND dateVisite BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                              "'"+ui->dateEdit_fin->date().toString("yyyy-MM-dd")+"' ");
                            }

            if(critereDate=="Date enregistrement"){
        requeteConsulter.exec("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                              "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                              "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                              "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                              "visite.idRuche=ruche.idRuche AND "
                              "visite.idTypeV=typeVisite.idTypeV AND "
                              "visite.idVisite=actionVisite.idVisite AND "
                              "actionVisite.idAction=actionEffectuee.idAction AND dateEnregistrement BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                              "'"+ui->dateEdit_fin->date().toString("yyyy-MM-dd")+"' ");
                            }
            modelConsulter->setQuery(requeteConsulter);
            afficherInfo2();
             }
}

void livraison::on_dateEdit_fin_dateChanged(const QDate &date)
{
    QString critereDate= ui->comboBox_critere->currentText();
    if(date < ui->dateEdit_debut->date()){
          QMessageBox::information(0,"ERREUR DE DATE","La date de fin ne peut pas être inférieur à la date de début ");
          ui->tableWidgetConsult->setRowCount(0);
          }
          else{
        if(critereDate=="Période"){
    requeteConsulter.exec("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                          "libelleAction , "
                          "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                          "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                          "visite.idRuche=ruche.idRuche AND "
                          "visite.idTypeV=typeVisite.idTypeV AND "
                          "visite.idVisite=actionVisite.idVisite AND "
                          "actionVisite.idAction=actionEffectuee.idAction AND dateVisite BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                          "'"+ui->dateEdit_debut->date().toString("yyyy-MM-dd")+"' ");
                        }

        if(critereDate=="Date enregistrement"){
    requeteConsulter.exec("SELECT numRuche, dateVisite, heureVisite, libelleTypeV, "
                          "GROUP_CONCAT(DISTINCT libelleAction ORDER BY libelleAction ASC SEPARATOR ', ') AS ActionsEffectuees, "
                          "QteLitreRecolte, QteKiloCire, QtePollen, QteKiloPropolis,constat, recommandation, visite.idVisite FROM "
                          "visite,actionVisite,ruche,actionEffectuee,typeVisite WHERE "
                          "visite.idRuche=ruche.idRuche AND "
                          "visite.idTypeV=typeVisite.idTypeV AND "
                          "visite.idVisite=actionVisite.idVisite AND "
                          "actionVisite.idAction=actionEffectuee.idAction AND dateEnregistrement BETWEEN '"+date.toString("yyyy-MM-dd")+"' AND "
                          "'"+ui->dateEdit_debut->date().toString("yyyy-MM-dd")+"' ");
                        }
        modelConsulter->setQuery(requeteConsulter);
        afficherInfo2();
           }
}

void livraison::affich()
{

    QStringList entete2;
    ui->tableWidget_total->setColumnCount(4);
    entete2<<"TOTAL LITRES RECOLTES"<<"TOTAL KILO DE CIRE"<<"TOTAL QTE POLLEN"<<"TOTAL KILO PROPOLIS";
    ui->tableWidget_total->setHorizontalHeaderLabels(entete2);


    ui->tableWidget_total->setRowCount(1);
    /*for (int i=0; i<requeteSomme.size(); i++)
    {

    QTableWidgetItem *itemsom = new QTableWidgetItem(modeleSomme->record(i).value(0).toString());
    itemsom->setFlags(Qt::ItemIsEnabled);
    itemsom->setTextAlignment(Qt::AlignCenter);
    ui->tableWidget_total->setItem(i,0,itemsom);
     QMessageBox::critical(this,"erreur sélection information",requeteSomme.value(0).toString());

    }*/

    OutilsMimes::customizeTableWidget(ui->tableWidget_total);
}


void livraison::on_pushButtonImprimerConsultation_clicked()
{
    QPrinter* printer = new  QPrinter();
    printer->setOrientation(QPrinter::Landscape);
    printer->setPaperSize(QPrinter::A4);
    QPrintPreviewDialog* apercuDialog = new QPrintPreviewDialog(printer,this);
    apercuDialog->setGeometry(10,40,1366,705);
    connect(apercuDialog, SIGNAL(paintRequested(QPrinter*)), this, SLOT(afficherApercuListe(QPrinter*)));
    apercuDialog->exec();
}

void livraison::afficherApercuListe(QPrinter *printer)
{
  QString titre="liste des abeilles";
  if(ui->comboBox_critere->currentText()=="Période")
      titre+=" du "+ui->dateEdit_debut->date().toString("dd/MM/yyyy")+" au "+ui->dateEdit_fin->date().toString("dd/MM/yyyy");
  else if(ui->comboBox_critere->currentText()=="Date de livraison")
      titre+=" du "+ui->dateEdit_debut->date().toString("dd/MM/yyyy");
  else if(ui->comboBox_critere->currentText()=="")
      titre+="  "+ui->comboBox_liste_Critere->currentText();
  /*else if(ui->comboBox_critere->currentText()=="Batiment")
      titre+=" du batiment "+ui->comboBox_liste_Critere->currentText();
  else if (ui->comboBox_critere->currentText()=="Service de traitement")
      titre+=" traités par le service : "+ui->comboBox_liste_Critere->currentText();
  else if(ui->comboBox_critere->currentText()=="Destinataire")
      titre+=" addressé au "+ui->comboBox_liste_Critere->currentText();
  else if(ui->comboBox_critere->currentText()=="Nombre de pièces jointes")
      titre+=" ayant"+ui->comboBox_liste_Critere->currentText()+" pièces jointes";
  else if(ui->comboBox_critere->currentText()=="Réference")
      titre+=" de  référence "+ui->comboBox_liste_Critere->currentText();
  else if(ui->comboBox_critere->currentText()=="Nature")
      titre+=" de nature "+ui->comboBox_liste_Critere->currentText();
 // OutilsMimes::afficherApercuImpression(modelImpression, titre,"", printer);

  QString basPage = "\n\t\tTotal plateaux normaux livrés: "+ui->tableWidget_total->item(0,0)->text()+"\t\t"+
                      "Total plateaux cassés livrés: "+ui->tableWidget_total->item(0,1)->text();*/

  OutilsMimes::afficherApercuImpression(ui->tableWidgetConsult,titre,"",printer,"");
}

void livraison::on_pushButton_Ajouter_clicked()
{
    int newRow = ui->tableWidget_ActionEffectuees->rowCount();
     ui->tableWidget_ActionEffectuees->insertRow(newRow);
     OPLComboBox* comboBox_AE = new OPLComboBox();
     QSqlQueryModel* modelAE = new QSqlQueryModel();
     modelAE->setQuery("SELECT DISTINCT LibelleAction FROM actionEffectuee");
     comboBox_AE->setModel(modelAE);
     ui->tableWidget_ActionEffectuees->setCellWidget(newRow, 0, comboBox_AE);
}

void livraison::on_pushButton_Retirer_clicked()
{
    ui->tableWidget_ActionEffectuees->removeRow(ui->tableWidget_ActionEffectuees->currentRow());
}

void livraison::on_pushButtonImprimerfiche_clicked()
{
    //indexOperation=ui->tableWidgetcons->selectionModel()->currentIndex().row();
    QPrinter* printer = new  QPrinter();
    printer->setOrientation(QPrinter::Portrait);
    printer->setPaperSize(QPrinter::A4);
    printer->setOutputFileName("C:/AFIP/ficheIdentification.pdf");
    QPrintPreviewDialog* apercuDialog = new QPrintPreviewDialog(printer,this);
    apercuDialog->setGeometry(10,40,1366,705);
    connect(apercuDialog, SIGNAL(paintRequested(QPrinter*)), this, SLOT(afficherApercuFiche(QPrinter*)));
    apercuDialog->exec();
}

void livraison::afficherApercuFiche(QPrinter *printer)
{
    QTextBrowser * editor = new QTextBrowser();
        editor->insertHtml("<style>"
                           "h1{text-align: center;font-family: Times New Roman; font-size: 18pt}"
                           "h2{text-align:center; font: \"Arial\"; color: black}"
                           ".logo{float: left;} .partie2{text-align:left; clear:both;}"
                           ".photo{float: right;}"
                           ".partie3{text-align:right; clear:both; }</style>"
                                   "<p style = \"text-align: center;font-family: Times New Roman; font-size: 18pt\">FICHE DE TERRAIN N°...........</p><br/>"
                                                  "<p text-align: justify style = \"line-height:30px;font-family: Times New Roman; font-size: 14pt\">"
                                             "NUMERO DE LA RUCHE: ................................................................<br/> "
                                             "DATE DE VISITE: ........................................................................<br/>"
                                             "HEURE DE VISITE: .......................................................................<br/> "
                                             "TYPE DE VISITE: .........................................................................<br/> "
                                             "<input type=\"checkbox\" cols=\"1\">"
                                                  "<libelle>ACTIONS EFFECTUEES</libelle><br/> "
                                                  "<option valeur=\"fr\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;Désherbage autour de la ruche</option><br/>"
                                                  " <option valeur=\"nl\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;Vérification du toit</option><br/>"
                                                  "<option valeur=\"en\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;Nettoyage des lattes</option><br/>"
                                                  "<option valeur=\"other\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;Vérification des bouteilles</option><br/>"
                                                  "<option valeur=\"en\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;Vérification des abreuvoirs</option><br/>"
                                                  "<option valeur=\"other\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;Nettoyage des toiles d'araignée</option><br/>"
                                                  "<option valeur=\"en\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;Collage des orifices indésirables</option><br/><br/> "
                                               "</input>"
                                             "NOMBRE DE LITRES RECOLTES: ......................................................<br/> "
                                             "NOMBRE DE KILOS DE CIRE: .........................................................<br/>"
                                             "QUANTITE DE POLLEN: .............................................................<br/>"
                                             "NOMBRE DE KILO DE PROPOLIS:.......................................................<br/>"
                                             "CONSTAT DE VISITE: ................................................................"
                                                                " ............................................................."
                                                                "..............................................................<br/>"
                                             "RECOMMANDATIONS: ................................................................."
                                                               "................................................................"
                                                               "................................................................<br/></p>"
                                            );

    editor->print(printer);
}

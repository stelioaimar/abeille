-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 25 mai 2021 à 17:33
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `abeille`
--

-- --------------------------------------------------------

--
-- Structure de la table `actioneffectuee`
--

DROP TABLE IF EXISTS `actioneffectuee`;
CREATE TABLE IF NOT EXISTS `actioneffectuee` (
  `idAction` int(11) NOT NULL AUTO_INCREMENT,
  `libelleAction` varchar(50) NOT NULL,
  PRIMARY KEY (`idAction`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `actioneffectuee`
--

INSERT INTO `actioneffectuee` (`idAction`, `libelleAction`) VALUES
(1, 'Désherbage autour de la ruche'),
(2, 'vérification du toit'),
(3, 'nettoyage des lattes'),
(4, 'vérification des bouteilles '),
(5, 'vérification des abreuvoirs'),
(6, 'collage des orifices indésirables '),
(7, 'nettoyage des toiles d\'araignée ');

-- --------------------------------------------------------

--
-- Structure de la table `ruche`
--

DROP TABLE IF EXISTS `ruche`;
CREATE TABLE IF NOT EXISTS `ruche` (
  `idRuche` int(11) NOT NULL AUTO_INCREMENT,
  `idTypeR` int(11) NOT NULL,
  `numRuche` varchar(10) NOT NULL,
  `dateInstallation` datetime NOT NULL,
  `dateColonisation` datetime NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`idRuche`),
  KEY `idTypeR` (`idTypeR`),
  KEY `idUser` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `typeruche`
--

DROP TABLE IF EXISTS `typeruche`;
CREATE TABLE IF NOT EXISTS `typeruche` (
  `idTypeR` int(11) NOT NULL AUTO_INCREMENT,
  `libelleTypeR` varchar(15) NOT NULL,
  PRIMARY KEY (`idTypeR`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typeruche`
--

INSERT INTO `typeruche` (`idTypeR`, `libelleTypeR`) VALUES
(1, 'colonise'),
(2, 'non colonise');

-- --------------------------------------------------------

--
-- Structure de la table `typevisite`
--

DROP TABLE IF EXISTS `typevisite`;
CREATE TABLE IF NOT EXISTS `typevisite` (
  `idTypeV` int(11) NOT NULL AUTO_INCREMENT,
  `libelleTypeV` varchar(25) NOT NULL,
  PRIMARY KEY (`idTypeV`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typevisite`
--

INSERT INTO `typevisite` (`idTypeV`, `libelleTypeV`) VALUES
(1, 'ordinaire'),
(2, 'grande récolte'),
(3, 'récolte de nettoyage');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `nomPrenoms` varchar(50) NOT NULL,
  `login` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `profil` varchar(15) NOT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`idUser`, `nomPrenoms`, `login`, `password`, `profil`) VALUES
(1, 'dsa', 'dsa', 'mimes', 'Administrateur'),
(2, 'dsa', 'dsa', 'mimes2', 'modificateur'),
(3, 'Charles TOKO', 'Mr Le maire', 'configPK', 'modificateur');

-- --------------------------------------------------------

--
-- Structure de la table `visite`
--

DROP TABLE IF EXISTS `visite`;
CREATE TABLE IF NOT EXISTS `visite` (
  `idVisite` int(11) NOT NULL AUTO_INCREMENT,
  `idTypeV` int(11) NOT NULL,
  `dateVisite` date NOT NULL,
  `heureVisite` time NOT NULL,
  `idAction` int(11) NOT NULL,
  `constat` varchar(100) NOT NULL,
  `recommandation` varchar(100) NOT NULL,
  `QteLitreRecolte` double NOT NULL,
  `QteKiloCire` double NOT NULL,
  `QtePollen` double NOT NULL,
  `QteKiloPropolis` double NOT NULL,
  `dateEnregistrement` datetime NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`idVisite`),
  KEY `idTypeV` (`idTypeV`),
  KEY `visite_ibfk_2` (`idAction`),
  KEY `idUser` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `ruche`
--
ALTER TABLE `ruche`
  ADD CONSTRAINT `ruche_ibfk_1` FOREIGN KEY (`idTypeR`) REFERENCES `typeruche` (`idTypeR`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ruche_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `visite`
--
ALTER TABLE `visite`
  ADD CONSTRAINT `visite_ibfk_1` FOREIGN KEY (`idTypeV`) REFERENCES `typevisite` (`idTypeV`) ON UPDATE CASCADE,
  ADD CONSTRAINT `visite_ibfk_2` FOREIGN KEY (`idAction`) REFERENCES `actioneffectuee` (`idAction`) ON UPDATE CASCADE,
  ADD CONSTRAINT `visite_ibfk_3` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

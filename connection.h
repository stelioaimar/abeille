#ifndef CONNECTION_H
#define CONNECTION_H
#include <QMessageBox>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

static bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("127.0.0.1");
    db.setUserName("dsa");
    db.setPassword("mimes02");
    db.setDatabaseName("abeille");
    db.setPort(3306);

    if (!db.open()) {
        QString errorMsg = "Impossible de se connecter à la base de données\n";
        errorMsg.append(db.lastError().text());
                 errorMsg.append("\nCliquer sur \"Annuler\" pour sortir.");
        QMessageBox::critical(0, "Erreur de connexion",
            errorMsg, QMessageBox::Cancel);
        return false;
    }

    return true;
}

#endif // CONNECTION_H

#include "accueil.h"
#include <QApplication>
#include "connection.h"
#include "authentification.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    if(!createConnection())
        return 1;
    authentification w;
    w.show();

    return a.exec();
}
